extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var bgColor=Color(0.05,0.05,0.07)
var fgColor=Color(0.95,0.95,0.93)
var mkColor=Color(0.8,0.95,0.8)
var pcName


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func save():
	var file = File.new()
	file.open("user://Settings.json", File.WRITE)
	var settings={}
	settings["bgColor"]=bgColor
	settings["fgColor"]=fgColor
	settings["mkColor"]=mkColor
	settings["pcName"]=pcName
	file.store_string(to_json(settings))
	file.close()
	pass

func str_to_col(string):
	return Color(float(string.split(",")[0]),float(string.split(",")[1]),float(string.split(",")[2]))

func load():
	var filename="user://Settings.json"
	var file = File.new()
	print("loading settings...")
	if(file.file_exists(filename)):
		file.open(filename, File.READ)
		var settings=parse_json(file.get_as_text())
		bgColor=str_to_col(settings["bgColor"])
		fgColor=str_to_col(settings["fgColor"])
		mkColor=str_to_col(settings["mkColor"])
		pcName=settings["pcName"]
		file.close()
		print("done loading settings")
	else:
		print("settings not found")
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
