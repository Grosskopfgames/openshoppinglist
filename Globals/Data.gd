extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
const prodinfo = preload("res://Tabs/Database/ProduktInfos.tscn")
const slitem = preload("res://Tabs/Shoppinglist/ShoppingListItem.tscn")

var readable_data={"offline":[],"online":[]}
var offline_db_productinfos_temporary

var itemslist=[""]
var productlists=[[]]
var products=[[]]#ids,ids from importstuff
var current_product=0
var outdir=""

var currentquery=""
var todownload=""
var infodialog
var statusbar

# Called when the node enters the scene tree for the first time.
func _ready():
	if(global.db!=null):
		currentquery=global.actualquery+" #4 order by summval desc;"
		var isfirst=true
		for where_string in global.hardwheres:
			if(isfirst):
				currentquery=currentquery.replace("#4"," where directinfo.id in (select id from directinfo where "+global.hardwheres[where_string]+" #4)")
				isfirst=false
			else:
				currentquery=currentquery.replace("#4","and "+global.hardwheres[where_string]+" #4")
		
		currentquery=currentquery.replace("#4","")
		if not global.hasfilters:
			currentquery=currentquery.replace(" order by summval desc","")
		_updated_query(true)
	
	if(global.filterpositions!=[[],[],[]]):
		statusbar.get_node("Filters").updateFilters(global.filterpositions)
	if(global.selected_shops!=[[],[]]):
		statusbar.get_node("Shops").updateFilters(global.selected_shops)
	pass # Replace with function body.

func _reset_query(filters):
	currentquery=""
	#print("hasreset")
	_updated_query()

func _updated_query(_rightnow=false):
	if(global.db==null):
		return
	if(currentquery==""):
		currentquery=global.actualquery+" order by summval desc;"
		
		var isfirst=true
		for where_string in global.hardwheres:
			if(isfirst):
				currentquery=currentquery.replace("#4"," where directinfo.id in (select id from directinfo where "+global.hardwheres[where_string]+" #4)")
				isfirst=false
			else:
				currentquery=currentquery.replace("#4","and "+global.hardwheres[where_string]+" #4")
		
		currentquery=currentquery.replace("#4","")
		if not global.hasfilters:
			currentquery=currentquery.replace(" order by summval desc","")
	var actquery=currentquery
	if(actquery.find("#4")!=-1):
		var isfirst=true
		for where_string in global.hardwheres:
			if(isfirst):
				actquery=actquery.replace("#4"," where directinfo.id in (select id from directinfo where "+global.hardwheres[where_string]+" #4)")
				isfirst=false
			else:
				actquery=actquery.replace("#4","and "+global.hardwheres[where_string]+" #4")
		actquery=actquery.replace("#4","")
	todownload=actquery
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _download_data(query):#{todo: data.todownload,thr: thread}
	var toquery=query["todo"]
	offline_db_productinfos_temporary=global.db.fetch_array(toquery)
	readable_data["offline"]=offline_db_productinfos_temporary
	global.set_read_db()
	tabs.database.get_node("ScrollContainer").scroll_vertical=0
	for node in tabs.database.get_node("ScrollContainer/Optionen").get_children():
		node.queue_free()
	query["thr"].call_deferred("wait_to_finish")
	tabs.database.pagenum=0
	pass

func updateitems(text,args):
	current_product=args
	#print("updateitem "+text)
	itemslist[args]=text#when zutatenraw like \"%"+text+"%\" then 2 
	productlists[args]=global.actualquery+" #4 order by summval desc;"
	global.hardwheres["findstring"]="(name like \"%"+text+"%\" or beschreibung like \"% "+text+" %\" or beschreibung like \""+text+"_ %\" or beschreibung like \""+text+"\" or beschreibung like \""+text+" %\")"
	var isfirst=true
	for where_string in global.hardwheres:
		if(isfirst):
			productlists[args]=productlists[args].replace("#4"," where directinfo.id in (select id from directinfo where "+global.hardwheres[where_string]+" #4)")
			isfirst=false
		else:
			productlists[args]=productlists[args].replace("#4","and "+global.hardwheres[where_string]+" #4")
	
	productlists[args]=productlists[args].replace("#4","")
	if not global.hasfilters:
		productlists[args]=productlists[args].replace(" order by summval desc","")
	currentquery=productlists[args]
	#print("updateditem "+text)
	_updated_query()
	#exportshoppinglistitems()
#	order by case 
#		when name LIKE "%John%" then 1 
#		when name LIKE "%Doe%"  then 2 
#		else 3 
#	end
	pass

func selectitem(args,node):
	if(!productlists[args].begins_with("Select")):
		updateitems(productlists[args],args)
		current_product=args
		return
	currentquery=productlists[args]
	current_product=args
	#print("selected",args)
	_updated_query()
	pass


func exportshoppinglistitems():
	var elements=[{"itemlist":[]}]
	var toquery=[tabs.shoppinglist.get_node("VBoxContainer")]
	for listelement in tabs.shoppinglist.get_children():
		if("shopinglistitem" in listelement.get_groups()):
			toquery.append(listelement)
			if not (listelement.selectedshop in elements):
				elements.append({})
			elements[len(elements)-1]["name"]=(listelement.itemname)
			elements[len(elements)-1]["amount"]=(float(listelement.amount))
			elements[len(elements)-1]["at_shop"]=(listelement.selectedshop)
			elements[len(elements)-1]["itemlist"]=[]
	for itemnr in range(len(elements)):
		for item in toquery[itemnr].get_children():
			if("product" in item.get_groups()):
				elements[itemnr]["itemlist"].append({"database_id":item.id,"amount":float(item.amount)})
	var file = File.new()
	print("saving shoppinglist_info.json")
	file.open("user://shoppinglist_info.json",File.WRITE)
	file.store_string(to_json( elements))
	file.close()
	print("done saving shoppinglist_info.json")
	#for item in toquery.get_children():
	#	if("product" in item.get_groups()):
	#		elements[len(elements)-1].append(item.productname)
	pass

func importshoppinglistitems():
	var _filename="user://shoppinglist_info.json"
	var file = File.new()
	print("loading shoppinglist_info.json")
	if(!file.file_exists(_filename)):
		print("shoppinglist_info.json not found")
		return
	file.open(_filename,File.READ)
	var importstuff=parse_json(file.get_as_text())
	#var _query=global.actualquery+" where directinfo.id="
	if(importstuff==null):
		return
	for i in range(0,len(importstuff)):
		var newitem
		if(i!=0):
			newitem = slitem.instance()
			tabs.shoppinglist.add_child(newitem)
			newitem.itemname=importstuff[i]["name"]
			newitem.amount=importstuff[i]["amount"]
			newitem.selectshop(importstuff[i]["at_shop"])
			newitem.setup(importstuff[i]["name"])
			itemslist.append(importstuff[i]["name"])
			productlists.append(importstuff[i]["name"])
			products.append([])
		for j in range(0,len(importstuff[i]["itemlist"])):
			var item=importstuff[i]["itemlist"][j]
			var queryid=-1
			for itemnum in range(len(readable_data["offline"])):# TODO this For-loop is unnecessary
				if readable_data["offline"][itemnum]["id"]==item["id"]:
					queryid=itemnum
					break
			if(queryid!=-1):
				var tmpprod=readable_data["offline"][queryid]
				var newpanel=prodinfo.instance()
				newitem.add_child(newpanel)
				newpanel.connect_all()
				newpanel.setvalues(tmpprod)
				newpanel.hideSelected()
				newpanel.amount=item["amount"]
				products[i].append(item["id"])
		tabs.shoppinglist._on_Einkaufsliste_resized()
	#for element in importstuff:
	#	if(element.)
	file.close()
	tabs.shoppinglist.updated_amount()
	print("done loading shoppinglist_info.json")
	#for item in toquery.get_children():
	#	if("product" in item.get_groups()):
	#		elements[len(elements)-1].append(item.productname)
	pass

func showinfos(id):
	
	var queryid=-1
	for itemnum in range(len(data.readable_data["offline"])):
		if data.readable_data["offline"][itemnum]["id"]==id:
			queryid=itemnum
	if queryid==-1:
		return
	var infos=data.readable_data["offline"][queryid]
	infodialog.showinfos(infos)
