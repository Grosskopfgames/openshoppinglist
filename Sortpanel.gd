extends Panel

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var prio
var Panelname setget setPanelName
var pressed=false
var presscoords=Vector2(0,0)
var smallsize=true
var logos={
	"Vegan":load("res://assets/Veganismus_logo.svg"),
	"Vegetarisch":load("res://assets/Vegetarian.svg"),
	"Fair":load("res://assets/FairTrade-Logo.svg"),
	"Bio":load("res://assets/allbio.svg"),
	"Bio+":load("res://assets/Bioplus.svg"),
	"Bio Plus":load("res://assets/bioplus.png"),
	"Bio_Plus":load("res://assets/bioplus.png"),
	"Unverpackt":load("res://assets/unverpackt-logo-mini.png"),
	"unverpackt":load("res://assets/unverpackt-logo-mini.png"),
	"Bosshammersch Hof":load("res://assets/Bosshammersch.png"),
	"Reformhaus":load("res://assets/Reformhaus_Logo_300dpiKopie2.png"),
	"Lokaso/Biomarkt NaturPur Siegen in Siegen":load("res://assets/biomarkt.png"),
	"Lozuka/Biomarkt":load("res://assets/biomarkt.png"),
	"Lozuka":load("res://assets/lozuka.png"),
	"Rewe":load("res://assets/rewe.png"),
	"REWE":load("res://assets/rewe.png"),
	"DM":load("res://assets/DM.png"),
	"Lozuka/Schneiders":load("res://assets/schneiders.png"),
	"Netto":load("res://assets/netto.png"),
	"Edeka":load("res://assets/edeka.png"),
	"EDEKA":load("res://assets/edeka.png"),
	"Rossmann":load("res://assets/rossmann.png"),
	"Aldi":load("res://assets/aldi.png"),
	"Aldi ":load("res://assets/aldi.png"),
	"Aldi Süd":load("res://assets/aldisud.png"),
	"Real":load("res://assets/Real.png"),
	"Kaufland":load("res://assets/Kaufland.png"),
	"Birkenhof":load("res://assets/birkenhof.png"),
	"Geld":load("res://assets/muenze.png"),
	"CO2eq":load("res://assets/wolke.png"),
	"Palmöl meiden":load("res://assets/palmoel.png"),
	}

signal inputOnPanel

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func setPanelName(name):
	if not smallsize:
		rect_size=Vector2(64,64)
		rect_min_size=Vector2(64,64)
		$Label.visible_characters=6
		global.connect("updatedquery",self,"updatehidden")
		#mouse_default_cursor_shape=Control.CURSOR_DRAG
		#$VSlider.visible=true
		prio=1.0-float(get_position_in_parent()-1)/float(get_parent().get_child_count())
		#$VSlider.value=prio
		#print(name)
	$Label.text=name
	hint_tooltip=name
	if(name in logos):
		$TextureRect.visible=true
		$TextureRect.texture=logos[name]
		$TextureRect/TextureRect.visible=true
		#print($TextureRect.rect_size)
		$Label.visible=false
	#else:
		#print(name+" has no logo")

func make_hidden_but_keep_scale():
	modulate=Color(1,1,1,0)
	hint_tooltip=""
	#for child in get_children():

func updatehidden(filterlist):
	#print(hint_tooltip)
	if(global.hardwheres.has(hint_tooltip)):
		$TextureRect2.visible=true
	else:
		$TextureRect2.visible=false
func _on_Panel_gui_input(event):
	if((event is InputEventMouseButton) ):
		#print("guiinput")
		emit_signal("inputOnPanel",self,event)
	pass # Replace with function body.

signal updatefiltervalue

func _on_VSlider_value_changed(value):
	if not smallsize:
		prio=value
		emit_signal("updatefiltervalue",self,value)
		#global.updatebasequery()
	pass # Replace with function body.
