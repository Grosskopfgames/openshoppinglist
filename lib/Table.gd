tool
extends HBoxContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
func _process(delta):
	var maxsize=0
	for child in get_children():
		for grandchild in child.get_children():
			if(maxsize<grandchild.rect_size.y):
				maxsize=grandchild.rect_size.y
	for child in get_children():
		for grandchild in child.get_children():
			grandchild.rect_size.y=maxsize

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
