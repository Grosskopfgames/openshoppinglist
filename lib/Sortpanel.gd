extends Panel

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var prio
var Panelname setget setPanelName
var pressed=false
var presscoords=Vector2(0,0)
var smallsize=true
var logos={
	"Vegan":load("res://assets/icons/Vegan.svg"),
	"Vegetarisch":load("res://assets/icons/Vegetarian.svg"),
	"Fair":load("res://assets/icons/Fairtrade.svg"),
	"Bio":load("res://assets/icons/AllBio.svg"),
	"Bio+":load("res://assets/icons/BioPlus.svg"),
	"Bio Plus":load("res://assets/icons/BioPlus.png"),
	"Bio_Plus":load("res://assets/icons/BioPlus.png"),
	"Unverpackt":load("res://assets/icons/unverpackt-logo-mini.png"),
	"unverpackt":load("res://assets/icons/unverpackt-logo-mini.png"),
	"Bosshammersch Hof":load("res://assets/icons/BosshammerschHof.png"),
	"Reformhaus":load("res://assets/icons/Reformhaus.png"),
	"Lokaso/Biomarkt NaturPur Siegen in Siegen":load("res://assets/icons/Biomarkt.png"),
	"Lozuka/Biomarkt":load("res://assets/icons/Biomarkt.png"),
	"Lozuka":load("res://assets/icons/Lozuka.png"),
	"Rewe":load("res://assets/icons/Rewe.png"),
	"REWE":load("res://assets/icons/Rewe.png"),
	"DM":load("res://assets/icons/Dm.png"),
	"Lozuka/Schneiders":load("res://assets/icons/Schneiders.png"),
	"Netto":load("res://assets/icons/Netto.png"),
	"Edeka":load("res://assets/icons/Edeka.png"),
	"EDEKA":load("res://assets/icons/Edeka.png"),
	"Rossmann":load("res://assets/icons/Rossmann.png"),
	"Aldi":load("res://assets/icons/Aldi.png"),
	"Aldi ":load("res://assets/icons/Aldi.png"),
	"Aldi Süd":load("res://assets/icons/AldiSued.png"),
	"Real":load("res://assets/icons/Real.png"),
	"Kaufland":load("res://assets/icons/Kaufland.png"),
	"Birkenhof":load("res://assets/icons/Birkenhof.png"),
	"Geld":load("res://assets/icons/Geld.png"),
	"CO2eq":load("res://assets/icons/Wolke.png"),
	"Palmöl meiden":load("res://assets/icons/Palmoelfrei.png"),
	}

signal inputOnPanel

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func setPanelName(name):
	if not smallsize:
		rect_size=Vector2(64,64)
		rect_min_size=Vector2(64,64)
		$Label.visible_characters=6
		global.connect("updatedquery",self,"updatehidden")
		#mouse_default_cursor_shape=Control.CURSOR_DRAG
		#$VSlider.visible=true
		prio=1.0-float(get_position_in_parent()-1)/float(get_parent().get_child_count())
		#$VSlider.value=prio
		#print(name)
	$Label.text=name
	hint_tooltip=name
	if(name in logos):
		$TextureRect.visible=true
		$TextureRect.texture=logos[name]
		$TextureRect/TextureRect.visible=true
		#print($TextureRect.rect_size)
		$Label.visible=false
	#else:
		#print(name+" has no logo")

func make_hidden_but_keep_scale():
	modulate=Color(1,1,1,0)
	hint_tooltip=""
	#for child in get_children():

func updatehidden(filterlist):
	#print(hint_tooltip)
	if(global.hardwheres.has(hint_tooltip)):
		$TextureRect2.visible=true
	else:
		$TextureRect2.visible=false
func _on_Panel_gui_input(event):
	if((event is InputEventMouseButton) ):
		#print("guiinput")
		emit_signal("inputOnPanel",self,event)
	pass # Replace with function body.

signal updatefiltervalue

func _on_VSlider_value_changed(value):
	if not smallsize:
		prio=value
		emit_signal("updatefiltervalue",self,value)
		#global.updatebasequery()
	pass # Replace with function body.
