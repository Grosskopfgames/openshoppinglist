extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var shopjson

var translation={"nameinfo":"name",
"idinfo":"id",
"contents":"zutatenraw",
"price":"preis",
"carbs":"kohy",
"protein":"eiweis",
"sugar":"zucker",
"fat":"fett",
"salt":"salz",

}


# Called when the node enters the scene tree for the first time.
func _ready():
	var shopinfos= File.new()
	shopinfos.open("res://Directsearch.json",shopinfos.READ)
	var shopstexts=shopinfos.get_as_text()
	shopjson=JSON.parse(shopstexts).result
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func get_elem_from_searchresult(inputconfig,textinput,elementsfound={}):
	if "key" in inputconfig:
		get_elem_from_children(inputconfig,textinput,elementsfound,inputconfig["key"])
	else:
		get_elem_from_children(inputconfig,textinput,elementsfound)
	return elementsfound

func get_elem_from_html(inputconfig,textinput,elementsfound={},key=""):
	var textoutputs=[]
	var notfirst=false
	for substr in textinput.split(inputconfig["start"]):
		if(notfirst):
			textoutputs.append(substr.split(inputconfig["end"])[0])
		notfirst=true
	
	for textoutput_all in textoutputs:
		#print("html")
		var elementsfound_in_this={}
		for conf_name in inputconfig:
			var textoutput=textoutput_all
			if("deletables" in inputconfig):
				for deletable in elementsfound["deletables"]:
					textoutput.replace(deletable,"")
			if conf_name!="deletables" and conf_name!="start" and conf_name!="end" and conf_name!="type" and inputconfig[conf_name]["start"] in textoutput:
				elementsfound_in_this[conf_name]=textoutput.split(inputconfig[conf_name]["start"])[1].split(inputconfig[conf_name]["end"])[0].http_unescape()
				if(conf_name=="contents" and (not "co2" in elementsfound)):
					pass
					#TODO add CO2 equivalents
		if(key in elementsfound_in_this):
			if(not elementsfound_in_this[key] in elementsfound):
				elementsfound[elementsfound_in_this[key]]=elementsfound_in_this
			else:
				for elem in elementsfound_in_this:
					elementsfound[elementsfound_in_this[key]][elem]=elementsfound_in_this[elem]
		else:
			for element in elementsfound_in_this:
				elementsfound[element]=elementsfound_in_this[element]
			pass
		#elementsfound=get_elem_from_children(inputconfig,textoutput,elementsfound)
	#var textoutputs=[child["start"]+textinput.split(child["start"])[1].split(child["end"])[0]+child["end"]]
	return elementsfound
	pass

func try_add_float(inputconfig,textinput,elementsfound,name_to_add):
	var possiblestring=textinput.split(inputconfig[name_to_add]["start"])[1].split(inputconfig[name_to_add]["end"])[0].http_unescape()
	if(possiblestring.is_valid_float()):
		elementsfound[name_to_add]=float(possiblestring)
	return elementsfound

func get_num_from_row(inputconfig,textinput,elementsfound):
	var substring=textinput.split(inputconfig["start"])[1].split(inputconfig["end"])[0].http_unescape()
	var textoutputs=[]
	var notfirst=false
	for substr in textinput.split(inputconfig["start"]):
		if(notfirst):
			textoutputs.append(substr.split(inputconfig["end"])[0])
		notfirst=true
	for textoutput in textoutputs:
		for conf_name in inputconfig:
			if conf_name!="start" and conf_name!="end" and conf_name!="type" and inputconfig[conf_name]["start"]in textoutput:
				try_add_float(inputconfig,textoutput,elementsfound,conf_name)
	return elementsfound

func get_tags_from_text(tags,text):
	var tags_out=[]
	for tag in tags:
		if(tags[tag] is Array):
			for tag_try in tags[tag]:
				if(tag_try in text):
					tags_out.append(tag)

func get_elem_from_json(inputconfig,textinput,elementsfound={},key=""):
	var textoutputs=[]
	var notfirst=false
	for substr in textinput.split(inputconfig["start"]):
		if(notfirst):
			textoutputs.append(substr.split(inputconfig["end"])[0])
		notfirst=true
	for textoutput in textoutputs:
		var outputjson=JSON.parse(textoutput).result
		var elementsfound_in_this={}
		for conf_name in inputconfig:
			if(not conf_name in ["start","end"] and inputconfig[conf_name] in outputjson and not inputconfig[conf_name] is Array):
				elementsfound_in_this[conf_name]=outputjson[inputconfig[conf_name]]
				if(elementsfound_in_this[conf_name] is String):
					elementsfound_in_this[conf_name]=elementsfound_in_this[conf_name].http_unescape()
			elif (not conf_name in ["start","end"] and inputconfig[conf_name] in outputjson):
				elementsfound_in_this[conf_name]=[]
				for elem in inputconfig[conf_name]:
					if(elem in outputjson):
						elementsfound_in_this[elem].append(outputjson[elem])
						if(elementsfound_in_this[elem] is String):
							elementsfound_in_this[elem]=elementsfound_in_this[elem].http_unescape()
		if(key in elementsfound_in_this):
			var name_=elementsfound_in_this[key]
			if(not elementsfound_in_this[key] in elementsfound):
				elementsfound[name_]=elementsfound_in_this
			else:
				for elem in elementsfound_in_this:
					elementsfound[name_][elem]=elementsfound_in_this[elem]
		else:
			for element in elementsfound_in_this:
				elementsfound[element]=elementsfound_in_this[element]
			pass
	return elementsfound

func get_elem_from_children(inputconfig,textinput,elementsfound={},key=""):
	for child in inputconfig["children"]:
		var elemfound_tmp={}
		if(key==""):
			elemfound_tmp=elementsfound
		#print(child)
		if(child["type"] == "html"):
			get_elem_from_html(child,textinput,elemfound_tmp,key)
		if(child["type"] == "rownum"):
			get_num_from_row(child,textinput,elemfound_tmp)
		if(child["type"] =="json"):
			get_elem_from_json(child,textinput,elemfound_tmp,key)
		if(key!=""):
			for elem in elemfound_tmp.keys():
				var act_elem_tmp=elemfound_tmp[elem]
				if(key in act_elem_tmp):
					if not(elem in elementsfound):
						elementsfound[elem]=act_elem_tmp
					else:
						for part in act_elem_tmp:
							elementsfound[elem][part]=act_elem_tmp[part]
	return elementsfound
func _on_HTTPRequest_request_completed(result, response_code, headers, body, shopname, req, elementsfound={}):
	print(response_code)
	#print("headers:")
	#print(headers)
	if(response_code==303):
		#print("Location:")
		for header in headers:
			if(str(header).begins_with("Location: ")):
				#print(str(header).right(10))
				req.request(str(header).right(10))
	
	#print("result:")
	#print(result)
	#print("body:")
	#print(body)
	if(response_code==200):
		var texts=body.get_string_from_utf8()
		var html_log=File.new()
		html_log.open("res://log/"+shopname+".html",File.WRITE)
		html_log.store_string(texts)
		html_log.close()
		elementsfound=get_elem_from_searchresult(shopjson[shopname],texts,elementsfound)
		for elem in elementsfound.keys():
			var actelem=elementsfound[elem]
			actelem["shop"]=shopname
			for part in actelem.keys():
				if(part in translation):
					actelem[translation[part]]=actelem[part]
					actelem.erase(part)
			data.readable_data["online"].append(actelem)
		print("Downloaded "+str(len(elementsfound))+" items in html-downloader")
	if(response_code>400):
		printerr("Download failed")
		#var lists=texts.split("<a class=\"product clearfix ff-tracking js-gtm-product-click\"")
		#$HBoxContainer3/RichTextLabel2.text="<a class=\"product clearfix ff-tracking js-gtm-product-click\""+lists[1].split("</a>")[0]+"</a>"
		#var ecommerceitems=texts.split("tag.ecommerce.displayProduct.products.set(")[1].split(");")[0]
		#print(str(JSON.parse(ecommerceitems).get_result()))
		#print(str(JSON.parse(body.get_string_from_utf8().split("dataLayer.push(")[1].split(");")[0]).get_result()["ecommerce"]["impressions"][0]))
	pass # Replace with function body.


func _on_Search_entered(text):
	var shopnames=["Netto"]
	for shopname in shopnames:
		var url=shopjson[shopname]["url"].replace("*",text)
		var req=HTTPRequest.new()
		self.add_child(req)
		req.connect("request_completed",self,"_on_HTTPRequest_request_completed",[shopname,req])
		data.readable_data["online"]=[]
		req.request(url)
		print("Html Downloader requesting "+url)
		#var error = req.request(url)
		#print("errorcode:")
		#print(error)
		pass # Replace with function body.

