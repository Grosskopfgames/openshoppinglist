extends VBoxContainer

onready var shops={"Lozuka":{"online":true,"active":false,"node":$OnlineShops/Panel},"Bosshammersch Hof":{"online":true,"active":false,"node":$OnlineShops/Panel2},"Birkenhof":{"online":true,"active":false,"node":$OnlineShops/Panel3}}

var shopid={"Rewe":"90","Real":"6","Lidl":"5","DM":"8","Reformhaus":"11","Lozuka/Schneiders":"23","Netto":"4","Edeka":"10","Aldi":"1","Aldi Süd":"2","Lozuka/Ebener":"18","Rossmann":"9","Kaufland":"3","Denns":"91","Lozuka/Biomarkt":"12","Unverpackt":"92","Birkenhof":"93","Eckseifen":"94","Bosshammersch Hof":"7","Lozuka":"12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26"}
var shopinfo = preload("res://Tabs/Shops/ShopInfo.tscn")
onready var shopParent=$ScrollContainer/OfflineShops
# Called when the node enters the scene tree for the first time.

func _enter_tree():
	tabs.shops=self

func _ready():
	#$VBoxContainer2/ViewportContainer/Viewport/Spatial/Camera.connect("_selected_shops",self,"updateshops")
	
	var file=File.new()
	print("loading usersettings_shops.json")
	if(file.file_exists("user://usersettings_shops.json")):
		file.open("user://usersettings_shops.json",file.READ)
		var tmpshops=parse_json(file.get_as_text())
		updateshops(tmpshops[0])
		for shopkey in tmpshops[0]:
			_on_Panel2_active_toggled(true,shopkey)
		updateshops(tmpshops[0])
		activateshopbuttons()
		file.close()
		print("done loading usersettings_shops.json")
	else:
		print("usersettings_shops.json not found")
	pass # Replace with function body.

func activateshopbuttons():
	for shop in shops:
		if(shops[shop]["active"]):
			if(shops[shop]["node"]==null):
				return false
			shops[shop]["node"].get_node("Control/CheckBox").pressed=true
	return true


func updateshops(shoplist):
	var toremove=[]
	for shop in shops:
		if (not shops[shop]["online"]) and (not shops[shop]["active"]) :
			shopParent.remove_child(shops[shop]["node"])
			toremove.append(shop)
	for shop in toremove:
		shops.erase(shop)
	for shop in shoplist:
		if not shops.has(shop):
			var newinfo=shopinfo.instance()
			shopParent.add_child(newinfo)
			newinfo.connect("active_toggled",self,"_on_Panel2_active_toggled")
			newinfo.get_node("Control/Label").text=shop
			shops[shop]={"online":false,"active":false,"node":newinfo}
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Panel2_active_toggled( active_toggle, name_toggle):
	shops[name_toggle]["active"]=active_toggle
	var first=true
	var shopquery=""
	var shops_out=[[],[]]
	for shop in shops:
		if(shops[shop]["active"] and shop in shopid):
			if(first):
				shopquery=" ladenid in ("
			first=false
			shopquery+=" "+shopid[shop]+", "
			shops_out[0].append(shop)
			shops_out[1].append(len(shops_out))
			#shops_out[shop]=len(shops_out)
	if(shopquery!=""):
		global.hardwheres["shops"]=shopquery.left(shopquery.find_last(","))+")"
	elif("shops" in global.hardwheres):
		global.hardwheres.erase("shops")
	#print(shops_out)
	global.selected_shops=shops_out
	
	var file =File.new()
	file.open("user://usersettings_shops.json",file.WRITE)
	file.store_string(to_json(shops_out))
	file.close()
	#hardwheres["shops"]=" shopid in "
	pass # Replace with function body.
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_ShoplistToggler_toggled(button_pressed):
	
	visible=button_pressed
	get_parent().get_node("MapTab").visible=!button_pressed
	if(button_pressed):
		get_parent().get_node("ShoplistToggler").text=">"
	else:
		get_parent().get_node("ShoplistToggler").text="<"
	pass # Replace with function body.
