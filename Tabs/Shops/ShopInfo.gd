extends Panel

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
# Called when the node enters the scene tree for the first time.
func _ready():
	$Control/CheckBox.modulate=GlobalSettings.fgColor
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

signal active_toggled

func _on_CheckBox_toggled(button_pressed):
	if(button_pressed):
		$Control/CheckBox.text="aktiv"
	if(not button_pressed):
		$Control/CheckBox.text="inaktiv"
	emit_signal("active_toggled",button_pressed,$Control/Label.text)
	pass # Replace with function body.
