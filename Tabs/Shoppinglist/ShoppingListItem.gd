extends VBoxContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var selectedshop=""
var itemname=""
var amount=0.0 setget setamount

# Called when the node enters the scene tree for the first time.
func _ready():
	$HBoxContainer/Label3.updateFilters(global.selected_shops)
	pass # Replace with function body.

func setup(name):
	
	$HBoxContainer/LineEdit.text=name
	connect("itemname_updated",data,"updateitems")
	connect("lineEdit_focus_entered",data,"selectitem")
	connect("dismiss",data,"DismissItem")
	connect("selectedshop_updated",data,"exportshoppinglistitems")
	global.connect("updated_shops",$HBoxContainer/Label3,"updateFilters")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

signal dismiss
signal itemname_updated
signal lineEdit_focus_entered
signal selectedshop_updated

func setamount(am):
	amount=am
	if($HBoxContainer/LineEdit2.text!=str(amount)):
		$HBoxContainer/LineEdit2.text=str(amount)

func selectshop(name):
	for i in range(len(global.selected_shops[0])):
		if(global.selected_shops[0][i]==name):
			var nodenum=$HBoxContainer/Label3.filters[0].find(name)
			if(nodenum!=-1):
				var node= $HBoxContainer/Label3.filters[1][nodenum]
				$HBoxContainer/Label3.input_done(node,null)

func _on_TextureButton_pressed():
	tabs.shoppinglist.DismissItem(self)
	#emit_signal("dismiss",self)
	pass # Replace with function body.


func _on_Label3_selected(number):
	selectedshop=global.selected_shops[0][number]
	emit_signal("selectedshop_updated")
	pass # Replace with function body.


func _on_LineEdit_text_entered(new_text):
	itemname=new_text
	emit_signal("itemname_updated",new_text,get_position_in_parent()-1)
	pass # Replace with function body.


func _on_LineEdit_focus_entered():
	emit_signal("lineEdit_focus_entered",get_position_in_parent()-1,self)
	pass # Replace with function body.


func _on_LineEdit2_text_entered(new_text):
	var value=float(new_text)
	if(value!=0 or new_text!="0"):
		amount=value
	else:
		$HBoxContainer/LineEdit2.text=str(amount)
	get_parent().updated_amount()
	pass # Replace with function body.


func _on_LineEdit_text_changed(new_text):
	_on_LineEdit_text_entered(new_text)
	pass # Replace with function body.
