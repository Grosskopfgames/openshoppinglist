extends ScrollContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal searchingItem
signal exportSL
signal clearSL
signal amountupdated

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


#func _on_ContaineredShoppinglistTab_searchingItem(_name):
#	emit_signal("searchingItem",_name)
#	pass # Replace with function body.


func _on_ContaineredShoppinglistTab_exportSL():
	emit_signal("exportSL")
	pass # Replace with function body.


func _on_ContaineredShoppinglistTab_clearSL():
	emit_signal("clearSL")
	pass # Replace with function body.


func _on_ContaineredShoppinglistTab_amountupdated():
	emit_signal("amountupdated")
	pass # Replace with function body.


func _on_resized_shoppinglist():
	tabs.shoppinglist.rect_position.y=0
	tabs.shoppinglist.rect_size.x=rect_size.x
	$Control/ViewportContainer/Viewport.size.y=max(tabs.shoppinglist.rect_size.y,rect_size.y)
	$Control/ViewportContainer/Viewport.size.x=rect_size.x
	$Control/ViewportContainer.rect_min_size.y=max(tabs.shoppinglist.rect_size.y,rect_size.y)
	$Control/ViewportContainer.rect_min_size.x=rect_size.x
	$Control/ViewportContainer/Viewport/TextureRect.rect_size.y=max(tabs.shoppinglist.rect_size.y,rect_size.y)
	$Control/ViewportContainer/Viewport/TextureRect.rect_size.x=rect_size.x
	$Control.rect_min_size.y=tabs.shoppinglist.rect_size.y
	minimum_size_changed()
	tabs.shoppinglist.rect_position.y=0
	pass # Replace with function body.


