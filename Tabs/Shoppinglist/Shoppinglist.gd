extends VBoxContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
#signal searchingItem

func _enter_tree():
	tabs.shoppinglist=self

func _ready():
	$HBoxContainer.modulate=GlobalSettings.fgColor

#func _on_TextEdit_text_changed(new_text):
#	emit_signal("searchingItem",new_text)
	pass # Replace with function body.


#func _on_TextEdit_focus_entered():
#	emit_signal("searchingItem",$VBoxContainer/HBoxContainer/TextEdit.text)
#	pass # Replace with function body.
signal amountupdated

func updated_amount():
	var totalsum=0.0
	for child in $VBoxContainer.get_children():
		if(child.is_in_group("product")):
			totalsum+=child.price*child.amount
	for child in get_children():
		if(child.is_in_group("shopinglistitem")):
			if(child.amount!=0.0):
				totalsum+=child.amount
			else:
				for grandchild in child.get_children():
					if(grandchild.is_in_group("product")):
						totalsum+=grandchild.price*grandchild.amount
		
	round(totalsum*100)/100
	$VBoxContainer/HBoxContainer/Label.text=str(totalsum)+"€"
	emit_signal("amountupdated")
	pass

#signal addSLI
func _on_AddButton_pressed():
	#emit_signal("addSLI")
	AddItem()
	pass # Replace with function body.

signal exportSL
func _on_ExportButton_pressed():
	emit_signal("exportSL")
	pass # Replace with function body.

signal clearSL
func _on_ClearButton_pressed():
	emit_signal("clearSL")
	pass # Replace with function body.



func AddItem():
	var newitem = data.slitem.instance()
	tabs.shoppinglist.add_child(newitem)
	#newitem.get_node("HBoxContainer/LineEdit").connect("text_entered",self,"updateitems",[len(itemslist)])
	#newitem.get_node("HBoxContainer/LineEdit").connect("focus_entered",self,"selectitem")
	newitem.connect("lineEdit_focus_entered",self,"selectitem")
	newitem.connect("itemname_updated",self,"updateitems")
	newitem.connect("focus_entered",self,"selectitem")
	newitem.connect("dismiss",self,"DismissItem")
	global.connect("updated_shops",newitem.get_node("HBoxContainer/Label3"),"updateFilters")
	data.itemslist.append("")
	data.productlists.append("")
	data.products.append([])
	_on_Einkaufsliste_resized()
	data.exportshoppinglistitems()
	pass # Replace with function body.


signal resized_shoppinglist
func _on_Einkaufsliste_resized():
	emit_signal("resized_shoppinglist")
	#$VBoxContainer/Products/Panel/Control/ViewportContainer/Viewport/TextureRect.rect_min_size.x=$VBoxContainer/Products.rect_size.x
	pass # Replace with function body.

func DismissItem(node):
	
	data.currentquery=global.actualquery+" #4 order by summval desc;"
	var isfirst=true
	for where_string in global.hardwheres:
		if(isfirst):
			data.currentquery=data.currentquery.replace("#4"," where directinfo.id in (select id from directinfo where "+global.hardwheres[where_string]+" #4)")
			isfirst=false
		else:
			data.currentquery=data.currentquery.replace("#4","and "+global.hardwheres[where_string]+" #4")
	
	data.currentquery=data.currentquery.replace("#4","")
	if not global.hasfilters:
		data.currentquery=data.currentquery.replace(" order by summval desc","")
	global.hardwheres.erase("findstring")
	data._updated_query()
	node.get_parent().remove_child(node)
	data.current_product=0
	#TODO reconnect those for the others :)
	#newitem.get_node("HBoxContainer/LineEdit").connect("text_entered",self,"updateitems",[len(itemslist)])
	#newitem.get_node("HBoxContainer/LineEdit").connect("focus_entered",self,"selectitem",[len(itemslist),newitem])
	data.exportshoppinglistitems()
	rect_size.y=0
	tabs.shoppinglist.updated_amount()


func toggleitem(id, toggled):
	print(data.current_product)
	var parent=null
	parent = tabs.shoppinglist.get_child(data.current_product+1)
	for child in parent.get_children():
		if("product" in child.get_groups() and child.id==id and toggled==true):
			return
	#	if("shopinglistitem" in child.get_groups() and child.id==current_product):
	#		parent=child
	#if(parent==null):
	#	parent=$VBoxContainer/Products/Panel/Einkaufsliste
	if(toggled):
		var queryid=-1
		for itemnum in range(len(data.readable_data["offline"])):
			if data.readable_data["offline"][itemnum]["id"]==id:#TODO make offline_db acchievable by dictionary
				queryid=itemnum
		var tmpprod=data.readable_data["offline"][queryid]
		var newpanel=data.prodinfo.instance()
		parent.add_child(newpanel)
		newpanel.connect("pressedInfo",data,"showinfos")
		newpanel.connect("removed_product",data,"exportshoppinglistitems")
		newpanel.connect("removed_product",tabs.shoppinglist,"updated_amount")
		newpanel.connect("label_evaluated",tabs.mainwindow,"label_evaluated")
		#newpanel.connect("toggledInfo",self,"toggleitem")
		#productinfo_node.append(newpanel)
		newpanel.setvalues(tmpprod)
		newpanel.hideSelected()
		data.products[data.current_product].append(id)
		tabs.shoppinglist.updated_amount()
	else:
		for child in parent.get_children():
			var groups=child.get_groups()
			if("product" in groups and child.id==id):
				parent.remove_child(child)
				break
		data.products[data.current_product].remove(id)
	data.exportshoppinglistitems()
	pass
