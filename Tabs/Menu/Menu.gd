extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var version=""
var urls=[]


func _enter_tree():
	tabs.menu=self

# Called when the node enters the scene tree for the first time.
func _ready():
	var urlFile=File.new()
	urlFile.open("res://urls.txt",File.READ)
	urls=urlFile.get_as_text().split("\n")
	
	if(global.db!=null):
		var file=File.new()
		file.open("user://version.txt",File.READ)
		version=file.get_as_text()
		$VBoxContainer/Label.text="Version: "+version
		file.close()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button3_pressed():
	get_tree().quit()
	pass # Replace with function body.

signal disable_everypony
signal enable_everypony

var updatefiles=[
	false,
	false,
	false
]


func _on_Button_pressed():
	updatefiles=[false,false,false]
	emit_signal("disable_everypony")
	$VBoxContainer/Label.text="Lade alles herunter"
	$version.request(urls[0])
	pass # Replace with function body.

func tryenable():
	if(updatefiles[0] and updatefiles[1] and updatefiles[2]):
		global.loadfiles()
		var file=File.new()
		file.open("user://version.txt",File.READ)
		$VBoxContainer/Label.text="Version: "+file.get_as_text()
		file.close()
		emit_signal("enable_everypony")

func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	var keyFile=File.new()
	keyFile.open("res://key.txt",File.READ)
	var warenkey=keyFile.get_line()
	#keyFile.close()
	var file=File.new()
	file.open_encrypted_with_pass("user://Waren_mined_encrypted.dat",File.READ,warenkey)
	var bytes=file.get_buffer(file.get_len())
	file.close()
	#print(len(bytes))
	var fileout=File.new()
	fileout.open("user://Waren_mined.db",File.WRITE)
	fileout.store_buffer(bytes)
	fileout.close()
	updatefiles[0]=true
	tryenable()
	#print($HTTPRequest.get_downloaded_bytes())
	pass # Replace with function body.


func _on_settings_config_request_completed(result, response_code, headers, body):
	updatefiles[1]=true
	tryenable()
	
	pass # Replace with function body.


func _on_filters_request_completed(result, response_code, headers, body):
	updatefiles[2]=true
	tryenable()
	pass # Replace with function body.


func _on_version_request_completed(result, response_code, headers, body):
	var file=File.new()
	file.open("user://version.txt",File.READ)
	var versionfound=file.get_line()
	if(updatefiles[0] and updatefiles[1] and updatefiles[2]):
		$VBoxContainer/Label.text="Version: "+versionfound
	else:
		if(version!=versionfound):
			$VBoxContainer/Label.text="Downloading ... Version: "+versionfound
			$waren_enc.request(urls[1])
			$settings_config.request(urls[2])
			$filters.request(urls[3])
	file.close()
	pass # Replace with function body.


func _on_Button4_pressed():
	$VBoxContainer/Label.text="Deleting everything"
	var dir=Directory.new()
	dir.remove("user://version.txt")
	dir.remove("user://filters.json")
	dir.remove("user://settings_config.json")
	dir.remove("user://Waren_mined_encrypted.dat")
	dir.remove("user://Waren_mined.db")
	$VBoxContainer/Label.text="done deleting everything"
	pass # Replace with function body.

