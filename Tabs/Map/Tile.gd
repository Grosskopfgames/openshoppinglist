extends TextureRect


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var file
export var pos= Vector3(8,5,4)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _get_tile():
	var testfile=File.new()
	file="map_"+str(pos.z)+"_"+str(pos.x)+"_"+str(pos.y)+".png"
	if(testfile.file_exists("user://"+file)):
		_load_file_offline()
		return
	var url="http://tile.openstreetmap.de/"+str(pos.z)+"/"+str(pos.x)+"/"+str(pos.y)+".png"
	#http.download(url, file, self, "_got_tile", [file])
	var http_request = HTTPRequest.new()
	add_child(http_request)
	http_request.download_file="user://"+file
	http_request.connect("request_completed", self, "_http_request_completed")
	# Perform the HTTP request. The URL below returns a PNG image as of writing.
	var error = http_request.request(url)
	if error != OK:
		push_error("An error occurred in the HTTP request.")
	pass # Replace with function body.

func _load_file_offline():
	
	var _image = Image.new()
	#var error = image.load_png_from_buffer(body)
	#if error != OK:
	#	push_error("Couldn't load the image.")
	_image.load("user://"+file)
	var texture_new = ImageTexture.new()
	texture_new.create_from_image(_image)
	self.texture=texture_new
	# Display the image in a TextureRect node.
	#var texture_rect = TextureRect.new()
	#add_child(texture_rect)
	#texture_rect.texture = texture

func _http_request_completed(result, response_code, headers, body):
	_load_file_offline()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
