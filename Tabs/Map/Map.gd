extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var tiles = load("res://Tabs/Map/Tile.tscn")
var dragging = false
var lastpos
var zoom=5
var latlongpos=Vector2(51.49154,10.39358)


func _enter_tree():
	tabs.map=self

# Called when the node enters the scene tree for the first time.
func _ready():
	
	#print(pos2latlong(Vector3(4,8,5)))
	#print(pos2url(latlong2pos(Vector2(50,10),10)))
	draw_map()
	resize_control()
	
	pass

func resize_control():
	$Camera2D/Control.rect_size=rect_size
	$Camera2D/Control.rect_position=rect_size*-0.5

func draw_map():
	var positioncursor=latlong2pos(latlongpos,zoom)
	#print("Positioncursor "+str(positioncursor))
	var heightnode=Control.new()
	heightnode.name=str(zoom)
	add_child(heightnode)
	heightnode.mouse_filter=Control.MOUSE_FILTER_IGNORE
	heightnode.show_behind_parent=true
	var amountw=ceil(get_viewport().size.x/256.0)+1
	var amounth=ceil(get_viewport().size.y/256.0)+1
	#print("amounth "+str(amounth))
	#print("amountw "+str(amountw))
	var diffpixels=Vector2(positioncursor.x-floor(positioncursor.x),positioncursor.y-floor(positioncursor.y))*256
	#print("diffpixels "+str(diffpixels))
	for i in range(-ceil(amountw/2),ceil(amountw/2)):
		#print("i is "+str(i))
		var x=floor(positioncursor.x)+i
		var columnnode=Control.new()
		columnnode.name=str(x)
		heightnode.add_child(columnnode)
		columnnode.rect_position=Vector2(256*i-diffpixels.x,0)
		for j in range(-ceil(amounth/2),ceil(amounth/2)):
			var y=floor(positioncursor.y)+j
			var tile = tiles.instance()
			columnnode.add_child(tile)
			tile.name=str(y)
			tile.mouse_filter=Control.MOUSE_FILTER_IGNORE
			tile.pos=Vector3(x,y,zoom)
			tile.rect_position=Vector2(0,256*j-diffpixels.y)
			tile._get_tile()

func _input(event):
	if event is InputEventMouseButton:
		if event.is_pressed():
			dragging = true
			lastpos = event.position
		else:
			dragging = false
			$Camera2D.position=Vector2(0,0)
			get_node(str(zoom)).free()
			draw_map()
	elif event is InputEventMouseMotion and dragging:
		var delta=(lastpos - event.position)
		$Camera2D.position += delta
		var currentpos=latlong2pos(latlongpos,zoom)
		var newpos=Vector2(currentpos.x,currentpos.y)+(delta/256)
		latlongpos=pos2latlong(Vector3(newpos.x,newpos.y,zoom))
		#print(latlongpos)
		lastpos=event.position
	if(event.is_action_pressed("ui_plus")):
		_on_ZoomIn_pressed()
	if(event.is_action_pressed("ui_minus")):
		_on_ZoomOut_pressed()
	pass

func pos2url(pos):
	return("http://tile.openstreetmap.de/"+str(floor(pos.z))+"/"+str(floor(pos.x))+"/"+str(floor(pos.y))+".png")

func pos2latlong(pos):
	
	var steps=pow(2,pos.z)
	var pos_x=(((pos.x)*360)/steps)-180
	var pos_y=180*(atan(sinh(PI*(1-((2*(pos.y))/steps))))/PI)
	return Vector2(pos_y,pos_x)

func latlong2pos(latlong,height):
	var n=pow(2,height)
	var pos_x = (float(latlong.y+180)/360.0)*n
	var latitude_rad=(latlong.x/180) * PI
	#var pos_y = floor((1-(log(tan((latlong.x*PI)/180.0)+(1/((cos(latlong.x)*PI)/180)))/PI))*pow(2,height-1))
	var pos_y = n * (1 - (log(tan(latitude_rad)+(1/cos(latitude_rad)))/PI))/2
	#(pos_x)
	#print(pos_y)
	return Vector3(pos_x,pos_y,height)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_ZoomIn_pressed():
	if(zoom<19):
		remove_child(get_node(str(zoom)))
		zoom+=1
		draw_map()
	pass # Replace with function body.


func _on_ZoomOut_pressed():
	if(zoom>4):
		remove_child(get_node(str(zoom)))
		zoom-=1
		draw_map()
	pass # Replace with function body.


func _on_LineEdit_text_entered(new_text):
	$HTTPRequest.request("https://nominatim.openstreetmap.org/search/"+new_text+"?format=geocodejson")
	pass # Replace with function body.


func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	var json = JSON.parse(body.get_string_from_utf8())
	latlongpos=Vector2(json.result.features[0].geometry.coordinates[1],json.result.features[0].geometry.coordinates[0])
	remove_child(get_node(str(zoom)))
	zoom=13
	draw_map()
	pass # Replace with function body.


func _on_RichTextLabel_meta_clicked(meta):
	OS.shell_open(meta)
	pass # Replace with function body.


func _on_Map_resized():
	resize_control()
	pass # Replace with function body.


func _on_Viewport_size_changed():
	rect_size=get_parent().size
	pass # Replace with function body.
