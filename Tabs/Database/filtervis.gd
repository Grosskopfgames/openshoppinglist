extends HBoxContainer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var panel = preload("res://Tabs/Filters/FilterPanel.tscn")

var filters=[[],[],[]]
var selectednum=-1
export var enable_select=false
export var hide_if_unknown=true
export var type=""
# Called when the node enters the scene tree for the first time.
func _ready():
	
	pass # Replace with function body.

func updateFilters(namelist):
	#print(namelist)
	for child in get_children():
		remove_child(child)
	filters=[[],[],[]]
	for number in range(len(namelist[0])):
		#var actnum=namelist.values().find(number)
		#if namelist[1][number]==value:
		updateFilter(namelist[0][number],0)

func updateFilter(name,value):
	var num=filters[0].find(name)
	#print(num)
	#print(name)
	if num==-1:
		var newpanel=panel.instance()
		add_child(newpanel)
		newpanel.Panelname=name
		filters[2].append(0)
		filters[1].append(newpanel)
		filters[0].append(name)
		#newpanel.updatehidden()
		#newpanel.connect("inputOnPanel",self,"_moveSignal")
		newpanel.connect("inputOnPanel",self,"input_done")
		#print(name,value)
		if(value>1/3):
			newpanel.modulate=Color(1.0,1.0,1.0)#0.5,1.0,0.5)
		elif(value<-1/3):
			if(hide_if_unknown):
				newpanel.get_node("TextureRect3").visible=true
				newpanel.hint_tooltip="nicht "+newpanel.hint_tooltip
			else:
				newpanel.modulate=Color(1.0,0.5,0.5)
		else:
			newpanel.modulate=Color(1.0,1.0,1.0)
			if(hide_if_unknown):
				newpanel.make_hidden_but_keep_scale()
				move_child(newpanel,0)
				#newpanel.hide()
	else:
		filters[1][num].updatehidden({})
		if(value>1/3):
			filters[1][num].modulate=Color(1.0,1.0,1.0)
			#filters[1][num].modulate=Color(0.5,1.0,0.5)
		elif(value<-1/3):
			#filters[1][num].modulate=Color(0.5,1.0,0.5)
			if(hide_if_unknown):
				filters[1][num].get_node("TextureRect3").visible=true
				filters[1][num].hint_tooltip="nicht "+filters[1][num].hint_tooltip
				pass
			else:
				filters[1][num].modulate=Color(0.5,1.0,0.5)
		else:
			filters[1][num].modulate=Color(1.0,1.0,1.0)
			if(hide_if_unknown):
				filters[1][num].make_hidden_but_keep_scale()
				move_child(filters[1][num],0)
				#filters[1][num].hide()
signal selected
signal label_evaluated
func input_done(node,event):
	if(enable_select):
		selectednum=filters[1].find(node)
		#print("selected ",selectednum)
		emit_signal("selected",selectednum)
		for filternum in range(len(filters[0])):
			if(filternum==selectednum):
				filters[1][filternum].modulate=Color(0.5,0.6,1.0)
			else:
				filters[1][filternum].modulate=Color(1.0,1.0,1.0)
	if(type=="productinfo" and event.pressed and BUTTON_LEFT==event.button_index and node.hint_tooltip!=""):
		selectednum=filters[1].find(node)
		var filtername=filters[0][selectednum]
		var productid=get_parent().get_parent().id
		var why=global.filters[filtername]["querywhy"]
		var whynot=global.filters[filtername]["querywhynot"]
		var queries=""
		var text_out=""
		if(!node.get_node("TextureRect3").visible):
			queries=why
			text_out="Dieses Produkt hat das Siegel "+filtername+", weil das Folgende Merkmal aufgefallen ist:"
		else:
			queries=whynot
			text_out="Dieses Produkt hat das Siegel "+filtername+" nicht, weil das Folgende Merkmal aufgefallen ist:"
		var querieslist=queries.replace("#3",str(productid)).split("\n Union\n")
		var results={"category":[],"traces":[],"ingredients":[],"allergens":[],"herstellerid":[],"shopid":[]}
		for query in querieslist:
			if(query.begins_with(" Select category")):
				results["category"]=global.db.fetch_array(query+";")
			elif(query.begins_with(" Select ingredient From contains Where  traces=0")):
				results["ingredients"]=global.db.fetch_array(query+";")
			elif(query.begins_with(" Select ingredient From contains Where  traces=1")):
				results["traces"]=global.db.fetch_array(query+";")
			elif(query.begins_with(" Select allergen")):
				results["allergens"]=global.db.fetch_array(query+";")
			elif(query.begins_with(" Select herstellerid")):
				results["herstellerid"]=global.db.fetch_array(query+";")
			elif(query.begins_with(" Select shopid")):
				results["shopid"]=global.db.fetch_array(query+";")
			else:
				print(query)
				print("this query is missing")
				pass
		for tmpres in results["category"]:
			text_out+="\n Kategorie: "+ global.db.fetch_array("Select name from category where id="+str(tmpres[0])+";")[0][0]
		for tmpres in results["ingredients"]:
			text_out+="\n Zutat: "+ global.db.fetch_array("Select name from ingredient where id="+str(tmpres[0])+";")[0][0]
		for tmpres in results["traces"]:
			text_out+="\n Spuren von: "+ global.db.fetch_array("Select name from ingredient where id="+str(tmpres[0])+";")[0][0]
		for tmpres in results["allergens"]:
			text_out+="\n Enthält allergen: "+ global.db.fetch_array("Select name from allergen where id="+str(tmpres[0])+";")[0][0]
		for tmpres in results["herstellerid"]:
			text_out+="\n Ist von Hersteller: "+ global.db.fetch_array("Select name from creator where id="+str(tmpres[0])+";")[0][0]
		for tmpres in results["shopid"]:
			text_out+="\n Ist von Laden: "+ global.db.fetch_array("Select name from shop where id="+str(tmpres[0])+";")[0][0]
		emit_signal("label_evaluated",text_out)
		pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
