extends VBoxContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var currentscroll=0
var pagenum=0.0

var thread
#var threadrunning=false
# Called when the node enters the scene tree for the first time.

func _enter_tree():
	tabs.database=self

func _ready():
	
	$LineEdit/TextureRect.modulate=GlobalSettings.fgColor
	thread=Thread.new()
	currentscroll=0
	pagenum=0
	if(global.dbopen):
		data.importshoppinglistitems()
	else:
		global.connect("opened_db",data,"importshoppinglistitems")
	pass # Replace with function body.

func start_thread():
	thread.start(data,"_download_data",{"todo": data.todownload,"thr": thread})

func _process(delta):
	if((!thread.is_active()) and data.todownload!=""):
		#print("heeey")
		print("starting database thread")
		if(global.dbopen):
			start_thread()
		else:
			if(!global.is_connected("opened_db",self,"start_thread")):
				global.connect("opened_db",self,"start_thread")
		data.todownload=""
	if(currentscroll!=get_node("ScrollContainer").scroll_vertical):
		currentscroll=get_node("ScrollContainer").scroll_vertical
	if(!thread.is_active() and data.readable_data["offline"]!=null and visible and 
	get_node("ScrollContainer").rect_size.y+currentscroll>=get_node("ScrollContainer/Optionen").rect_size.y):
		try_update_database_tab()

func try_update_database_tab():
	for i in range(pagenum*20,min(pagenum*20+20,len(data.readable_data["offline"])+len(data.readable_data["online"]))):
		#print("got ",i," from ",len(readable_data["offline"]))
		#for tmpprod in prodinfos:
		if(i in range(0,len(data.readable_data["offline"]))):
			
			var tmpprod=data.readable_data["offline"][i]
			var newpanel=data.prodinfo.instance()
			get_node("ScrollContainer/Optionen").add_child(newpanel)
			newpanel.connect("pressedInfo",self,"showinfos")
			newpanel.connect("toggledInfo",self,"toggleitem")
			newpanel.connect("label_evaluated",self,"label_evaluated")
			#productinfo_node.append(newpanel)
			newpanel.setvalues(tmpprod)
		else:
			var tmpprod=data.readable_data["online"][i-len(data.readable_data["offline"])]
			var newpanel=data.prodinfo.instance()
			get_node("ScrollContainer/Optionen").add_child(newpanel)
			newpanel.connect("pressedInfo",self,"showinfos")
			newpanel.connect("toggledInfo",self,"toggleitem")
			newpanel.connect("label_evaluated",self,"label_evaluated")
			newpanel.setvalues(tmpprod)
		#currentquery.replace()
	var countleft = len(range(pagenum*20,min(pagenum*20+20,len(data.readable_data["offline"])+len(data.readable_data["online"]))))
	if(countleft!=0):
		if(countleft>19):
			pagenum+=1
		else:
			var diffpages=(countleft)/20.0
			pagenum+=diffpages
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _exit_tree():
	if(thread.is_active()):
		thread.wait_to_finish()



func _on_DatabaseToggler_toggled(button_pressed):
	visible=button_pressed
	get_parent().get_node("ShoppinglistTab").visible=!button_pressed
	if(button_pressed):
		get_parent().get_node("DatabaseToggler").text=">"
	else:
		get_parent().get_node("DatabaseToggler").text="<"
	pass # Replace with function body.


func _on_Search_changed(new_text):
	data.updateitems(new_text,0)
	pass # Replace with function body.
