extends Panel

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var id=-1
var productname
var productshop
var amount=1.0 setget setamount
var price=0.0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func connect_all():
	connect("pressedInfo",data,"showinfos")
	connect("removed_product",data,"exportshoppinglistitems")
	connect("removed_product",tabs.shoppinglist,"updated_amount")
	connect("label_evaluated",data,"label_evaluated")

func setamount(am):
	amount=am
	if($Control/LineEdit.text!=str(amount)):
		$Control/LineEdit.text=str(amount)

func setSelected(toggle):
	$Control/TextureButton.pressed=toggle

func hideSelected():
	$Control/TextureButton.visible=false
	$Control/TextureButton2.visible=true
	$Control/LineEdit.visible=true
	$Control/Label3.visible=true

func setvalues(dictionary):
	for key in dictionary:
		if(typeof(key)==TYPE_STRING):
			if(key=="id"):
				id=dictionary["id"]
			if(key=="shop"):
				$Control/Panel.setPanelName(dictionary["shop"].replace("Lozuka/","").replace("Nord",""))
				productshop=dictionary["shop"].replace("Nord","")
			if(key=="name"):
				$Control/Label.text=dictionary["name"]
				$Control.hint_tooltip=dictionary["name"]
				productname=dictionary["name"]
			if(key=="preis"):
				$Control/VBoxContainer/Label.text=str(dictionary["preis"])+" €"
				price=dictionary["preis"]
			if(key=="menge"):
				if(dictionary["menge"]>0.5):
					$Control/VBoxContainer/Label2.text=str(round(dictionary["menge"]*100)/100)+" Kg"
				else:
					$Control/VBoxContainer/Label2.text=str( round(dictionary["menge"]*100000)/100)+" g"
			if(key=="co2" and dictionary["co2"]!=-1 and dictionary["co2"]!=15870):
				$Control/Label2.text=str(dictionary["co2"]/1000)+"\ng CO2/g"
				$Control/Label2.hint_tooltip="CO2 Fußabdruck pro Kilo anhand von Daten des Öko instituts geschätzt"
				if(dictionary["co2"]==0):
					$Control/Label2.hint_tooltip="Der Hersteller kompensiert bekanntermaßen den CO2 Fußabdruck seiner Produkte"
			if(key.ends_with("_val") and not(key=="CO2eq_val" or key=="Geld_val")):
				$Control/FiltersList.updateFilter(key.left(len(key)-4),dictionary[key])
#	if "filters" in dictionary:
#		for filter in dictionary["filters"]:
#			$Control/Label3.addFilter(filter)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

signal pressedInfo

func _on_Panel_gui_input(event):
	
	if( event.is_pressed() and event.button_index == BUTTON_LEFT):
		#print("guiinput")
		#emit_signal("pressedInfo",id)
		data.showinfos(id)
	pass # Replace with function body.

signal toggledInfo

func _on_TextureButton_toggled(button_pressed):
	#print("guiinput2")
	emit_signal("toggledInfo",id,true)
	pass # Replace with function body.


signal removed_product

func _on_TextureButton2_pressed():
	get_parent().remove_child(self)
	emit_signal("removed_product")
	#free()
	pass # Replace with function body.


func _on_TextureButton_pressed():
	#emit_signal("toggledInfo",id,true)
	tabs.shoppinglist.toggleitem(id,true)
	pass # Replace with function body.


func _on_LineEdit_text_entered(new_text):
	var value=float(new_text)
	if(value!=0 or new_text!="0"):
		amount=value
	else:
		$Control/LineEdit.text=str(amount)
	if(get_parent().is_in_group("shopinglistitem")):
		get_parent().get_parent().updated_amount()
	else:
		get_parent().get_parent().updated_amount()
	pass # Replace with function body.

signal label_evaluated
func _on_FiltersList_label_evaluated(var text):
	emit_signal("label_evaluated",text)
	pass # Replace with function body.
