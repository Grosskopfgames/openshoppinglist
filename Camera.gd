extends Camera

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var from
var to
var enabled_raycast=false
var firsthitpos

var lastcircle

var shops=[]

var viewportrect

var areainfo = preload("res://Area.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
const ray_length = 1000

func _input(event):
	if(event.is_action("scrollin")):
		size=max(size-1,10)
		var mouseposition = project_ray_origin(event.position)
		var mousefactor=(60-size)/60
		#var mousefactor= 1-max(max(size/2+abs(mouseposition.x),size/2+abs(mouseposition.z))/30-1,0)
		#print(mousefactor)
		translation.x=mouseposition.x*mousefactor
		translation.z=mouseposition.z*mousefactor
		#mouseposition
		#print("at ",event.position)
		#print("cam at ",project_ray_origin(event.position))
		pass
	if(event.is_action("scrollout")):
		size=min(size+1,60)
		var mouseposition = project_ray_origin(event.position)
		var mousefactor=(60-size)/60
		#var mousefactor= max(max(size/2+abs(mouseposition.x),size/2+abs(mouseposition.z))/30-1,0)
		#var mousefactor= 1-max(max(size/2+abs(mouseposition.x),size/2+abs(mouseposition.z))/30-1,0)
		#print(mouseposition*mousefactor)
		#print(mousefactor)
		translation.x=mouseposition.x*mousefactor
		translation.z=mouseposition.z*mousefactor
		
		pass
	if( event is InputEventMouse and get_viewport().get_visible_rect().has_point(event.position)):
		#if(event.position.x>minimumpos.x)
		if(Input.is_action_just_pressed("mousepress")):
			var camera = self
			from = camera.project_ray_origin(event.position)
			to = from + camera.project_ray_normal(event.position) * ray_length
			enabled_raycast=true
			var cylinder=areainfo.instance()
			get_parent().add_child(cylinder)
			if(lastcircle!=null):
				get_parent().remove_child(lastcircle)
			lastcircle=cylinder
			lastcircle.translation=Vector3(0,0,-200)
			#lastcircle.visible=false
			shops=[]
			lastcircle.connect("found_shop",self,"_add_shop")
		if(not Input.is_action_just_pressed("mousepress") and Input.is_action_pressed("mousepress")):
			if(lastcircle!=null and firsthitpos!=null):
				lastcircle.translation=firsthitpos
				var distance =project_ray_origin(event.position).distance_to(from)
				lastcircle.radius=distance
				lastcircle.visible=true
			
			
		if(Input.is_action_just_released("mousepress")):
			if(lastcircle!=null and firsthitpos!=null):
				#lastcircle.translation=firsthitpos
				#var distance =min(10.0,project_ray_origin(event.position).distance_to(from))
				#lastcircle.radius=distance
				emit_signal("_selected_shops",shops)
			

signal _selected_shops

func _add_shop(shopname):
	if(not shopname in shops):
		shops.append(shopname)

func _physics_process(delta):
	if(enabled_raycast):
		var space_state = get_world().direct_space_state
		var result = space_state.intersect_ray(from, to)
		if result:
			enabled_raycast=false
			firsthitpos=result.position
			lastcircle.radius=0.5
			lastcircle.translation=Vector3(firsthitpos)
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
