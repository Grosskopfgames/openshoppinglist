extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

#var productinfo_node=[]

#onready var shoppinglist=get_node("VBoxContainer/Products/Panel/Control/ViewportContainer/Viewport/Einkaufsliste")

func _enter_tree():
	tabs.mainwindow=self

# Called when the node enters the scene tree for the first time.
func _ready():
	_on_Node2D_resized()
	$Window/Body/ShoppinglistTab._on_resized_shoppinglist()
	pass # Replace with function body.
	$Window/Header/Menu/MenuButton.get_popup().connect("id_pressed", self, "_on_Help_MenuButton_pressed")
	$Window/Body/MenuTab.connect("enable_everypony",data,"_reset_query")
	global.connect("updatedquery", data, "_reset_query")
	global.connect("updated_shops", data, "_reset_query")
	global.connect("updatedquery",$Window/Header/Statusbar/Filters,"updateFilters")
	global.connect("updated_shops",$Window/Header/Statusbar/Shops,"updateFilters")
	$Window/Header/Searchbar/TextureRect.modulate=GlobalSettings.fgColor
	data.infodialog=$Dialogs/WindowDialog
	data.statusbar=$Window/Header/Statusbar
	#print($WindowDialog/HBoxContainer/Control/VBoxContainer/Preis/Label.text)
	
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
###Call this when filters changed

func _on_MenuButton_pressed(var value):
	var headline=$Window/Header/Statusbar/Label
	var header=$Window/Header/Statusbar
	var searchbar=$Window/Header/Searchbar
	var buttonbar=$Window/Header/Menu
	
	tabs.menu.visible=false;
	tabs.filtermenu.visible=false;
	$Window/Body/DatabaseToggler.visible=false;
	tabs.database.visible=false;
	$Window/Body/ShoppinglistTab.visible=false;
	tabs.shoppinglist.get_parent().gui_disable_input=true;
	tabs.map.get_parent().gui_disable_input=true;
	$Window/Body/MapTab.visible=false
	$Window/Body/ShoplistToggler.visible=false;
	tabs.shops.visible=false;
	searchbar.visible=false;
	header.visible=false;
	var isVertical=OS.get_window_safe_area().size.x<=OS.get_window_safe_area().size.y
	match(value):
		0:
			_on_MenuButton_pressed(-1)
		1:
			buttonbar.set_selected("Configbutton")
			headline.text="Einstellungen"
			tabs.menu.visible=true;
			header.visible=true;
		-1:
			buttonbar.set_selected("Shoppinglistbutton")
			headline.text="Einkaufsliste"
			$Window/Body/DatabaseToggler.visible=isVertical;
			tabs.database.visible=$Window/Body/DatabaseToggler.pressed or !isVertical;
			$Window/Body/ShoppinglistTab.visible=!$Window/Body/DatabaseToggler.pressed or !isVertical;
			if(!$Dialogs/FileDialog.visible):
				tabs.shoppinglist.get_parent().gui_disable_input=!$Window/Body/ShoppinglistTab.visible;
			header.visible=true;
		2:
			buttonbar.set_selected("Shopsbutton")
			headline.text="Märkte"
			$Window/Body/MapTab.visible=!$Window/Body/ShoplistToggler.pressed or !isVertical
			tabs.map.get_parent().gui_disable_input=!$Window/Body/MapTab.visible;
			$Window/Body/ShoplistToggler.visible=isVertical;
			tabs.shops.visible=$Window/Body/ShoplistToggler.pressed or !isVertical;
			header.visible=true;
			#$VBoxContainer/Shopsinfos/VBoxContainer2/ViewportContainer/Viewport/Spatial/Camera.resetcam()
		3:
			buttonbar.set_selected("Profilebutton")
			tabs.filtermenu.visible=true;
			searchbar.visible=true;
	pass # Replace with function body.


func _on_TextureButton_pressed():
	_on_MenuButton_pressed(-1);
	pass # Replace with function body.


func _Menu_Button(extra_arg_0):
	#print(extra_arg_0)
	match(extra_arg_0):
		"settings":
			_on_MenuButton_pressed(1)
		"profiles":
			_on_MenuButton_pressed(3)
		"shops":
			_on_MenuButton_pressed(2)
	pass # Replace with function body.



func _on_TextureButton4_pressed():
	OS.shell_open("mailto:request_fix.opensl@grosskopfgames.de")
	pass # Replace with function body.


func _on_TextureButton5_pressed():
	OS.shell_open("https://t.me/kingcreole")
	pass # Replace with function body.

func _on_Help_MenuButton_pressed(id):
	if(id==0):
		_on_TextureButton4_pressed()
	else:
		_on_TextureButton5_pressed()


#func _on_Menu_disable_everypony():
#	toggle_everypony(true)
#	pass # Replace with function body.


#func _on_Menu_enable_everypony():
#	toggle_everypony(false)
#	pass # Replace with function body.

#func toggle_everypony(bool_in):
#	$VBoxContainer/Searchbar_Tags/TextureRect2/MenuButton.disabled=bool_in
#	$VBoxContainer/Searchbar_Tags/TextureRect2/TextureButton.disabled=bool_in
#	$VBoxContainer/Searchbar_Tags/TextureRect2/TextureButton4.disabled=bool_in
#	$VBoxContainer/Searchbar_Tags/TextureRect2/TextureButton5.disabled=bool_in
#	$VBoxContainer/Searchbar_Tags/TextureRect2/TextureButton6.disabled=bool_in
#	$VBoxContainer/Searchbar_Tags/TextureRect2/TextureButton7.disabled=bool_in
#	$VBoxContainer/Searchbar_Tags/TextureRect2/TextureButton8.disabled=bool_in




func _on_FileDialog_file_selected(path):
	if($Window/Body/ShoppinglistTab.visible):
		tabs.shoppinglist.get_parent().gui_disable_input=false
	#path=path.replace(".txtxt",".txt")
	if(path.ends_with(".txt")):
		
		var result=""
		var elements={}
		var toquery=[tabs.shoppinglist.get_node("VBoxContainer")]
		for listelement in tabs.shoppinglist.get_children():
			if("shopinglistitem" in listelement.get_groups()):
				toquery.append(listelement)
				if not (listelement.selectedshop in elements):
					elements[listelement.selectedshop]=[]
				elements[listelement.selectedshop].append([listelement.itemname,listelement.amount])
		for item in toquery:
			for product in item.get_children():
				
				if("product" in product.get_groups()):
					if(not product.productshop in elements):
						elements[product.productshop]=[]
					elements[product.productshop].append([product.productname,product.amount])
		for element in elements:
			if(element ==""):
				result+="von egal woher\n"
			else:
				result+="Von "+element+"\n"
			for item in elements[element]:
				result+=str(item[1])+" Einheiten "+item[0]+"\n"
		var file=File.new()
		file.open(data.outdir+"/"+path,File.WRITE)
		file.store_string(result)
		file.close()
	elif(path.ends_with(".png")):
		var img = tabs.shoppinglist.get_parent().get_texture().get_data()
		img.flip_y()
		img.save_png(path)
	elif(path.ends_with(".exr")):
		var img = tabs.shoppinglist.get_parent().get_texture().get_data()
		img.flip_y()
		img.save_exr(path)
		pass # Replace with function body.




func _on_FileDialog_dir_selected(dir):
	data.outdir=dir
	pass # Replace with function body.



#func _on_Control_resized():
#	tabs.shoppinglist.get_parent().get_parent().rect_size.x=$VBoxContainer/Products/Panel/Control.rect_size.x
#	$VBoxContainer/Products/Panel/Control/ViewportContainer/Viewport/TextureRect.rect_min_size.x=$VBoxContainer/Products.rect_size.x
#	tabs.shoppinglist.rect_size.x=$VBoxContainer/Products/Panel/Control.rect_size.x
#	#print("viewportsize: ",$VBoxContainer/Products/Panel/Control/ViewportContainer.rect_size)
#	#print("pos1: ",$VBoxContainer/Products/Panel/Control/ViewportContainer.rect_size)
#	#print("pos2size: ",$VBoxContainer/Products/Panel/Control.rect_size)
#	#print("pos3size: ",shoppinglist.rect_size)
#	pass # Replace with function body.


func _on_FileDialog_popup_hide():
	if($Window/Body/ShoppinglistTab.visible):
		tabs.shoppinglist.get_parent().gui_disable_input=false
	pass # Replace with function body.


func _on_clear_pressed():
	var toquery=[tabs.shoppinglist.get_node("VBoxContainer")]
	for listelement in tabs.shoppinglist.get_children():
		if("shopinglistitem" in listelement.get_groups()):
			toquery.append(listelement)
	for nodenr in range(len(toquery)):
		if(nodenr==0):
			for child in toquery[0].get_children():
				if(child.get_position_in_parent()!=0):
					toquery[0].remove_child(child)
		else:
			tabs.shoppinglist.remove_child(toquery[nodenr])
	data._reset_query(null)
	data.exportshoppinglistitems()
	pass # Replace with function body.


func _on_Panel3_active_toggled():
	pass # Replace with function body.


#func _on_Einkaufsliste_searchingItem(text):
#	data.updateitems(text,0)
#	pass # Replace with function body.


func _on_Einkaufsliste_amountupdated():
	data.exportshoppinglistitems()
	pass # Replace with function body.



func _on_Einkaufsliste_exportSL():
	
	tabs.shoppinglist.get_parent().gui_disable_input=true
	$Dialogs/FileDialog.popup()
func label_evaluated(var text):
	$Dialogs/Siegeldialog.dialog_text=text
	$Dialogs/Siegeldialog.popup_centered()


func _on_Node2D_resized():
	if(OS.get_window_safe_area().size.x>OS.get_window_safe_area().size.y):
		var shoplv=$Window/Body/MapTab.visible or $Window/Body/ShoplistTab.visible
		var datablv=$Window/Body/ShoppinglistTab.visible or $Window/Body/DatabaseTab.visible
		$Window/Body/ShoplistToggler.pressed=true
		$Window/Body/ShoplistToggler.visible=false
		$Window/Body/DatabaseToggler.pressed=true
		$Window/Body/DatabaseToggler.visible=false
		$Window/Body/DatabaseTab.visible=datablv
		$Window/Body/ShoppinglistTab.visible=datablv
		$Window/Body/ShoplistTab.visible=shoplv
		$Window/Body/MapTab.visible=shoplv
	else:
		$Window/Body/ShoplistToggler.visible=$Window/Body/MapTab.visible or $Window/Body/ShoplistTab.visible
		$Window/Body/DatabaseToggler.visible=$Window/Body/ShoppinglistTab.visible or $Window/Body/DatabaseTab.visible
		$Window/Body/DatabaseTab.visible=$Window/Body/DatabaseToggler.pressed and $Window/Body/DatabaseToggler.visible
		$Window/Body/ShoppinglistTab.visible=(!$Window/Body/DatabaseToggler.pressed) and $Window/Body/DatabaseToggler.visible
		$Window/Body/ShoplistTab.visible=$Window/Body/ShoplistToggler.pressed and $Window/Body/ShoplistToggler.visible
		$Window/Body/MapTab.visible=(!$Window/Body/ShoplistToggler.pressed) and $Window/Body/ShoplistToggler.visible
		tabs.shoppinglist.get_parent().gui_disable_input=!$Window/Body/ShoppinglistTab.visible
		tabs.map.get_parent().gui_disable_input=!$Window/Body/MapTab.visible
	pass # Replace with function body.
