extends HBoxContainer

var panel = preload("res://FilterPanel.tscn")

var filters=[[],[],[]]

func addFilter(name,factor=-1):
	#if not name in filters[0] or filters[2][filters[0].find(name)]==0.0:
	var newpanel=panel.instance()
	add_child(newpanel)
	newpanel.smallsize=false
	newpanel.Panelname=name
	filters[0].append(name)
	filters[1].append(newpanel)
	if(factor==-1):
		filters[2].append(0)#1.0-float(newpanel.get_position_in_parent()-1)/float(get_child_count()))
	else:
		filters[2].append(factor)
		newpanel.get_node("VSlider").value=factor
	#newpanel.connect("inputOnPanel",self,"_moveSignal")
	newpanel.connect("updatefiltervalue",self,"_updatefilter")
	newpanel.updatehidden({})
	global.updatebasequery(filters)

func removeFilter(name):
	if name in filters[0]:
		var number=filters[0].find(name)
		remove_child(filters[1][number])
		filters[0].remove(number)
		filters[1].remove(number)
		filters[2].remove(number)
		global.updatebasequery(filters)
func update_filter(name,value):
	if(filters[0].find(name)!=-1):
		#print("update"+name)
		#print(filters)
		_updatefilter(filters[1][filters[0].find(name)],value)

func _updatefilter(node,value):
	var position_diff=0
	var positionnode=filters[1].find(node)
	#print("filters")
	#print(filters)
	for i in range(0,positionnode):
		#print(i)
		#print("down at ",i)
		if(filters[2][i]<value):
			position_diff-=1
	for i in range(positionnode+1,len(filters[1])):
		#print("up at ",i)
		if(filters[2][i]>value):
			position_diff+=1
	filters[2][positionnode]=value
	#print("inbetween")
	#print(position_diff)
	#print(positionnode)
	if(position_diff!=0):
		
		if(position_diff>0):
			position_diff+=1
		move_child(node,positionnode+1+position_diff)
		if(len(filters[0])<=positionnode+position_diff):
			filters[0].append(filters[0][positionnode])
			filters[1].append(filters[1][positionnode])
			filters[2].append(filters[2][positionnode])
		else:
			filters[0].insert(positionnode+position_diff,filters[0][positionnode])
			filters[1].insert(positionnode+position_diff,filters[1][positionnode])
			filters[2].insert(positionnode+position_diff,filters[2][positionnode])
		
		#print("filtersout1")
		#print(filters)
		if(position_diff<0):
			filters[0].remove(positionnode+1)
			filters[1].remove(positionnode+1)
			filters[2].remove(positionnode+1)
		else:
			filters[0].remove(positionnode)
			filters[1].remove(positionnode)
			filters[2].remove(positionnode)
	
	#print("filtersout")
	#print(filters)
	global.updatebasequery(filters)
	node.updatehidden({})

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _moveSignal(node,event):
	
	if node.pressed:
		print("released")
		if event.button_index == 1:
			node.pressed=event.is_pressed()
			if !node.pressed:
				var distance = round((event.position.x-node.presscoords.x)/node.rect_size.x)
				var position = get_children().find(node)
				print(distance)
				print(position)
				if(distance+position<=get_child_count()):
					move_child(node,distance+position)
				#if(node.presscoords.x<event.position.x-node.rect_size.x/2):
				#	print("right")
				#elif(node.presscoords.x>event.position.x+node.rect_size.x/2):
				#	print("left")
				#else:
				#	print("center")
		#pressed=event.is_pressed()
	else:
		node.pressed=event.is_pressed()
	node.presscoords=event.position
	global.updatebasequery(filters)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
