extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

const SliderPreference = preload("res://Elemetpreference.tscn")
#const SwitchPreference = preload("res://Switchpreference.tscn")

#var Preferencetype={3:SliderPreference,2:SwitchPreference}
var setting_stored=[[],[],[]]

var hassetting=false

# Called when the node enters the scene tree for the first time.
func _ready():
	global.connect("finished_loading",self,"loadsettings")
	if(global.dbopen):
		var file =File.new()
		if(file.file_exists("user://usersettings_filter.txt")):
			file.open("user://usersettings_filter.txt",file.READ)
			setting_stored=parse_json(file.get_as_text())
			for setting in range(len(setting_stored[0])):
				if(setting_stored[1][setting]!=0):
					_changed_setting(setting_stored[0][setting],setting_stored[1][setting],setting_stored[2][setting])
			file.close()
		loadsettings()
	global.connect("updatedquery",self,"updatedsetting")
	pass # Replace with function body.

func loadsettings():
	var container=$TabContainer
	for key in global.settings.keys():
		var subcontainer=container.get_node(key).get_node("ScrollContainer/VBoxContainer")
		for subkey in global.settings[key].keys():
			var prefbox_settings=global.settings[key][subkey]
			var Prefbox_preload=SliderPreference#Preferencetype[min(prefbox_settings[2].size(),3)]
			var Prefbox=Prefbox_preload.instance()
			if(min(prefbox_settings[2].size(),3)<3):
				Prefbox.has_3_states=false
			Prefbox.Sliderlabels=prefbox_settings[2]
			Prefbox.namepref=subkey
			Prefbox.name=subkey
			if(global.settings[key][subkey][0] in setting_stored[0]):
				var numberpref=setting_stored[0].find(global.settings[key][subkey][0])
				if(setting_stored[1][numberpref]!=0):
					Prefbox.set_pref(setting_stored[2][numberpref])
					if(!hassetting):
						$HBoxContainer.visible=true
						hassetting=true
						$TabContainer.margin_bottom=-64
			Prefbox.tooltippref=prefbox_settings[1]
			Prefbox.Filter=prefbox_settings[0]
			Prefbox.connect("changedvalue",self,"_changed_setting")
			subcontainer.add_child(Prefbox)
	
func updatedsetting(filterspos):
	for name in setting_stored[0]:
		var number=filterspos[2].find(name)
		if(number!=-1):
			setting_stored[2]=filterspos[2]
	savesettings_to_disk()

func savesettings_to_disk():
	var file =File.new()
	file.open("user://usersettings_filter.txt",file.WRITE)
	file.store_string(to_json(setting_stored))
	file.close()

func _changed_setting(settingname,value,factor=-1.0):
	#print(factor)
	if(value==2):
		global.sethardwhere(settingname,true)
	else:
		global.sethardwhere(settingname,false)
	
	if(value!=0):
		var filternames=$HBoxContainer.filters[0]
		if(filternames.find(settingname)==-1 or(setting_stored[0].find(settingname)!=-1 and setting_stored[1][setting_stored[0].find(settingname)]==0)):
			#print("added "+settingname)
			_add_setting(settingname,factor)
		
	else:
		_remove_setting(settingname)
	
	if(setting_stored[0].find(settingname)!=-1):
		var settingnum=setting_stored[0].find(settingname)
		if(value!=0):
			setting_stored[1][settingnum]=value
			setting_stored[2][settingnum]=factor
			$HBoxContainer.update_filter(settingname,factor)
		else:
			setting_stored[0].remove(settingnum)
			setting_stored[1].remove(settingnum)
			setting_stored[2].remove(settingnum)
	else:
		setting_stored[0].append(settingname)
		setting_stored[1].append(value)
		if(factor!=-1.0):
			setting_stored[2].append(factor)
		else:
			setting_stored[2].append(0)#1.0-float(len(setting_stored[0])-1)/float($HBoxContainer.get_child_count()))
	savesettings_to_disk()
	

func _add_setting(settingname,factor):
	if(!hassetting):
		$HBoxContainer.visible=true
		$TabContainer.margin_bottom=-64
		hassetting=true
	$HBoxContainer.addFilter(settingname,factor)
	pass

func _remove_setting(settingname):
	if($HBoxContainer.get_child_count()==2):
		$TabContainer.margin_bottom=0
		hassetting=false
		$HBoxContainer.visible=false
	$HBoxContainer.removeFilter(settingname)
	pass

		# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_LineEdit_text_changed(new_text):
	for key in global.settings.keys():
		var subcontainer=$TabContainer.get_node(key).get_node("ScrollContainer/VBoxContainer")
		for subkey in global.settings[key].keys():
			var item=subcontainer.get_node(subkey)
			if(subkey.to_lower().find(new_text.to_lower()) and not new_text.length()==0):
				item.visible=false
			else:
				item.visible=true
	pass # Replace with function body.
