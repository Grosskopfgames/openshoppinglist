tool
extends HBoxContainer

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var Sliderlabels=["nicht relevant","anderes markieren und runtersortieren","anderes komplett ausblenden"]
var Filter setget setFilter
export (String) var namepref
export (String) var tooltippref
export (bool) var has_3_states

#export (Texture) var emptystar
#export (Texture) var halfstar
#export (Texture) var fullstar
#onready var textures=[emptystar,halfstar,fullstar]
var currentstatus=0
var currentval=0
var ready

signal changedvalue

func setFilter(filtername):
	Filter=filtername

# Called when the node enters the scene tree for the first time.
func _ready():
	#textures=[emptystar,halfstar,fullstar]
	#if(has_3_states):
		#emptystar=$VBoxContainer/HSlider.texture_normal
		#halfstar=$VBoxContainer/HSlider.texture_pressed
		#halfstar=$VBoxContainer/HSlider.texture_disabled
		#textures=[emptystar,halfstar,fullstar]
	$Label.text=namepref
	$Label.hint_tooltip=tooltippref
	hint_tooltip=tooltippref
	#$VBoxContainer/Label.text=Sliderlabels[int(0)]
	pass

func set_pref(value):
	$VBoxContainer/HSlider/Label.text=str(value)
	if(value!=0):
		$VBoxContainer/HSlider.value=value*100
		#if(not has_3_states):
		#	$VBoxContainer/CheckButton.pressed=true
		#else:
		#	currentstatus=value
			#updateTextures()
	if(has_3_states and value==1.0):
		$VBoxContainer/HSlider.value=101
		$VBoxContainer/Label.text=Sliderlabels[2]
	elif(value>0):
		$VBoxContainer/Label.text=Sliderlabels[1]
	else:
		$VBoxContainer/Label.text=Sliderlabels[0]
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func updateTextures():
	#print(currentstatus)
	var toset=null
	#if(textures!=null):
	#	toset=textures[currentstatus]
	if(toset==null):
		if(currentstatus==0):
			toset=$VBoxContainer/HSlider.texture_normal
		elif(currentstatus==1):
			toset=$VBoxContainer/HSlider.texture_pressed
		elif(currentstatus==2):
			toset=$VBoxContainer/HSlider.texture_disabled
	$VBoxContainer/HSlider.texture_pressed=toset
	$VBoxContainer/HSlider.texture_normal=toset
	$VBoxContainer/HSlider.texture_disabled=toset
	$VBoxContainer/HSlider.texture_hover=toset
	$VBoxContainer/HSlider.texture_focused=toset

func _on_pref_changed(value):
	$VBoxContainer/Label.text=Sliderlabels[int(value)]
	
	emit_signal("changedvalue",Filter,int(value),currentval)


func _on_CheckButton_toggled(button_pressed):
	_on_pref_changed(button_pressed)
	pass # Replace with function body.




func _on_HSlider_pressed():
	#print("pressed ",currentstatus)
	#currentstatus=int((currentstatus+1))%int(len(textures))
	#print("Filter: >"+str(Filter)+"<")
	_on_pref_changed(currentstatus)
	#updateTextures()
	pass # Replace with function body.


func _on_HSlider_value_changed(value):
	#print(value)
	if(value==0):
		currentstatus=0
	elif(value<=100 or (not has_3_states)):
		currentstatus=1
	else:
		currentstatus=2
	currentval=min(float(value)/100.0,1.0)
	$VBoxContainer/HSlider/Label.text=str(currentval)
	_on_pref_changed(currentstatus)
	
	pass # Replace with function body.
