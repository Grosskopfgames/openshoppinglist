extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var bgColor=Color(0.05,0.05,0.08)
export var fgColor=Color(0.95,0.95,0.92)
export var mkColor=Color(0.8,0.95,0.8)


# Called when the node enters the scene tree for the first time.
func _ready():
	GlobalSettings.load()
	if(GlobalSettings.pcName==null):
		set_color(bgColor,fgColor,mkColor)
		set_random_name()
	else:
		set_name(GlobalSettings.pcName)
		set_color(GlobalSettings.bgColor,GlobalSettings.fgColor,GlobalSettings.mkColor)
		get_tree().change_scene("res://MainScene.tscn")
	pass # Replace with function body.

func set_random_name():
	var _name="testname"
	var file = File.new()
	file.open("res://Data/Vornamen_2018_Koeln.txt", File.READ)
	var lines = file.get_as_text().split("\n")
	randomize()
	var linenum = (randi()%(len(lines)-1))+1
	var randname = lines[linenum].split(";")[1]
	var linenum2 = (randi()%(len(lines)-1))+1
	var randname2 = lines[linenum2].split(";")[1]
	_name=randname+" "+randname2
	set_name(_name)

func set_name(_name):
	GlobalSettings.pcName=_name
	$Background/V/H/Name.text=_name
	$Background/V/H2/Questions/Label.text="Darf "+_name+" auf den Webseiten der Supermärkten schauen?"
	$Background/V/H2/Questions/Label2.text="Darf "+_name+" die Daten von unseren Servern automatisch aktualisieren?"


func set_color(bgColor,fgColor,mkColor):
	GlobalSettings.bgColor=bgColor
	GlobalSettings.fgColor=fgColor
	GlobalSettings.mkColor=mkColor
	$Background.material.set_shader_param("bg_color",bgColor)
	add_color_override("font_color",fgColor)
	add_color_override("bg_color",bgColor)
	$Background/V/H/RandButton.modulate=fgColor
	$Background/V/H/InfoButton.modulate=fgColor
	$Background/V/H2/Answers/fgCol.color=fgColor
	$Background/V/H2/Answers/bgCol.color=bgColor
	theme.set_color("font_color","Label", fgColor)
	theme.set_color("font_color","LineEdit", fgColor)
	theme.set_color("font_color","Button", fgColor)
	#theme.set_color("font_color","CheckBox", fgColor)
	theme.set_color("font_color_fg","TabContainer", fgColor)
	#print(theme.get_color_list("Tabs"))
	theme.set_color("font_color_bg","TabContainer", fgColor.linear_interpolate(bgColor,0.5))
	#theme.set_color("font_color_hover","CheckBox", fgColor)
	#theme.set_color("font_color_pressed","CheckBox", mkColor)
	theme.get_stylebox("normal","Button").border_color=fgColor
	theme.get_stylebox("normal","LineEdit").border_color=fgColor
	theme.get_stylebox("normal","Button").bg_color=bgColor
	theme.get_stylebox("hover","Button").border_color=fgColor
	theme.get_stylebox("hover","Button").bg_color=bgColor
	theme.get_stylebox("pressed","Button").border_color=bgColor
	theme.get_stylebox("pressed","Button").bg_color=bgColor
	theme.get_stylebox("panel","PopupMenu").bg_color=bgColor
	theme.get_stylebox("panel","TooltipPanel").bg_color=bgColor
	theme.get_stylebox("panel","TooltipPanel").border_color=fgColor
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_RandButton_pressed():
	set_random_name()
	pass # Replace with function body.


func _on_InfoButton_pressed():
	$PopupDialog.popup()
	$Background.modulate=Color(0.4,0.4,0.4)
	pass # Replace with function body.


func _on_PopupDialog_popup_hide():
	$Background.modulate=Color(1,1,1)
	pass # Replace with function body.


func _on_ColorPickerButton2_color_changed(color):
	set_color(GlobalSettings.bgColor,color,GlobalSettings.mkColor)
	pass # Replace with function body.


func _on_ColorPickerButton_color_changed(color):
	set_color(color,GlobalSettings.fgColor,GlobalSettings.mkColor)
	pass # Replace with function body.


func _on_mkCol_color_changed(color):
	set_color(GlobalSettings.bgColor,GlobalSettings.fgColor,color)
	pass # Replace with function body.

func _on_Button_pressed():
	GlobalSettings.save()
	get_tree().change_scene("res://MainScene.tscn")
	pass # Replace with function body.

