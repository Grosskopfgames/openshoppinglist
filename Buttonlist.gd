extends HBoxContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var modulatecolor=GlobalSettings.mkColor
var fontcolor=GlobalSettings.fgColor

# Called when the node enters the scene tree for the first time.
func _ready():
	set_selected("Shoppinglistbutton")
	pass # Replace with function body.

func set_selected(_name):
	for child in get_children():
		if(child.name==_name):
			child.modulate=modulatecolor
		else:
			child.modulate=fontcolor
		

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
