extends Node

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

const SQLite = preload("res://lib/gdsqlite.gdns");
var db;
var filters_file = File.new()
var filters={}
var settings={}
var queries={}
var actualquery="Select directinfo.id as id, directinfo.name as name, directinfo.beschreibung as beschreibung, directinfo.zutatenraw as zutatenraw, directinfo.preis as preis, directinfo.menge as menge, shop.name as shop, creator.name as creator, directinfo.energie as energie, directinfo.kohlenhydrate as kohy, directinfo.eiweis as eiweis, directinfo.zucker as zucker, directinfo.fett as fett, directinfo.salz as salz, directinfo.carbon_footprint as co2 from directinfo left join shop on directinfo.ladenid=shop.id left join creator on directinfo.herstellerid=creator.id"
var hardwheres={}
var filterpositions=[[],[],[]]
var selected_shops=[[],[]] setget shops_select
var hasfilters=false
var sqlparams={"category":{"table":"part_of"},
"packaging":{"table":"packed_in"},
"ingredient":{"table":"contains","where":" traces=0 "},
"traces":{"table":"contains","where":" traces=1 "},
"allergen":{"table":"contains_allergen"},
"creator":{"table":"directinfo","row":"herstellerid"},
"shop":{"table":"directinfo","row":"ladenid"}}
var dbopen
signal updatedquery
# Called when the node enters the scene tree for the first time.
func _ready():
	var file=File.new()
	if(file.file_exists("user://Waren_mined.db")):
		loadfiles()
	#else:
		#file.close()
	pass # Replace with function body.

signal finished_loading

func loadfiles():
	
	var file=File.new()
	file.open("user://settings_config.txt",File.READ)
	settings=parse_json(file.get_as_text())
	file.close()
	db = SQLite.new();
	# Open item database
	dbopen=db.open("user://Waren_mined.db");
	loadfilters()
	loadSqlQueries()
	emit_signal("finished_loading")
	emit_signal("updatedquery")


func loadfilters():
	filters_file.open("user://filters.txt", File.READ)
	var nameelement=""
	var readingelement=false
	var elementparts={}
	var subelement=null
	var subelementtag
	if( filters_file.file_exists("user://filters.txt")):
		while(!filters_file.eof_reached()):
			#print("hi")
			var line=filters_file.get_line()
			if(readingelement==false and line!=""):
				nameelement=line
				readingelement=true
			elif(line!=""):
				if ': ' in line and subelement==null:
					elementparts[line.split(': ')[1]] = line.split(': ')[0]
				elif ': ' in line:
					subelement[line.split(': ')[1]]=line.split(': ')[0]
				else:
					if subelement!=null:
						elementparts[subelementtag]=subelement.duplicate()
					subelementtag=line
					subelement={}
			elif readingelement==true:
				if subelement!=null:
					elementparts[subelementtag]=subelement.duplicate()
				filters[nameelement] = elementparts.duplicate(true)
				elementparts={}
				readingelement=false
				subelement=null
		if(nameelement!="" and !elementparts.empty()):
			filters[nameelement] = elementparts.duplicate(true)

func loadSqlQueries():
	for setting in filters:
		var query=""
		var querypos=""
		var queryneg=""
		var querysort=""
		for param in filters[setting]:
			if(param in sqlparams):
				#, 1 as val
				var querytmppos="Select id From "+sqlparams[param]["table"]+" Where " 
				var querytmpneg="Select id From "+sqlparams[param]["table"]+" Where "
				if("where" in sqlparams[param]):
					querytmppos+= sqlparams[param]["where"]+"and "
					querytmpneg+= sqlparams[param]["where"]+"and "
				if("row" in sqlparams[param]):
					querytmppos+= sqlparams[param]["row"] + " In ( "
					querytmpneg+= sqlparams[param]["row"] + " In ( "
				else:
					querytmppos+= param + " In ( "
					querytmpneg+= param + " In ( "
				var haspos=false
				var hasneg=false
				for key in filters[setting][param].keys():#split up here
					if(filters[setting][param][key]=="1"):
						haspos=true
						querytmppos+=key+", "
					else:
						hasneg=true
						querytmpneg+=key+", "
				if(haspos):
					querytmppos=querytmppos.left(querytmppos.length()-2)
				if(hasneg):
					querytmpneg=querytmpneg.left(querytmpneg.length()-2)
				if(haspos):
#					var results=db.fetch_array(querytmppos+");")
#					print(results)
#					pass
					querypos=querypos+" )\n Union\n "+querytmppos
				if(hasneg):
					queryneg=queryneg+" )\n Union\n "+querytmpneg
					pass
			else:
				if param!="query":
					var _min=-1
					var _max=-1
					var _sortdir=0
					if("sort" in filters[setting][param].keys()):
						if filters[setting][param]["sort"]=="0":
							_sortdir=1
						elif filters[setting][param]["sort"]=="2":
							_sortdir=-1
					if("max" in filters[setting][param].keys()):
						_max=float(filters[setting][param]["max"])
					if("min" in filters[setting][param].keys()):
						_min=float(filters[setting][param]["min"])
					querysort=querysort+"Select id, "+str(_sortdir)+"* (1- 2*("+param#+value-_min/_max-min
					if(_min!=-1):
						querysort=querysort+"-"+str(_min)+") / ("
					else:
						querysort=querysort+"- 0) / ("
					if(_max!=-1):
						querysort=querysort+str(_max)+" - "
					else:
						if(param == "preis"):
							querysort=querysort+"20 - "
						elif(param=="carbon_footprint"):
							querysort=querysort+"25000 - "
						else:
							querysort=querysort+"100 - "
					if(_min!=-1):
						querysort=querysort+str(_min)+" )) "
					else:
						querysort=querysort+" 0)) "
					querysort+=" as val from directinfo  #4"
		querypos=querypos.right(querypos.find("S")-1)+" )"
		queryneg=queryneg.right(queryneg.find("S")-1)+" )"
		#if(queryneg!=" )"):
		#	queryneg="Select id, val From ( " + queryneg + ") WHERE id NOT In (Select id from( " + querypos+") )"
		if(querysort==""): #ONLY SORT OR POS/NEG
			#query="Select id, val FROM (Select id, 0 as val FROM directinfo) Right outer Join ( Select * from "+querypos+"\n AS pos Left OUTER Join\n "+queryneg+"as neg on not pos.id=neg.id ) using (id) "
			if(queryneg!=" )" and querypos!=" )"):
				query="Select id, case when id in ("+querypos+") then 1 when id in ("+queryneg+") then -1 else 0 end as val from directinfo #4"
			#	query="Select id, val FROM ("+querypos +" Union "+queryneg+") Union  Select id, 0 as val From directinfo WHERE id Not In (Select id FROM ("+querypos +" Union "+queryneg+"))"
			elif(querypos!=" )"):
				query="Select id, case when id in ("+querypos+") then 1 else 0 end as val from directinfo #4"
			#	query="Select id, val FROM ("+querypos +") Union  Select id, 0 as val From directinfo WHERE id Not In (Select id FROM ("+querypos +"))"
			elif(queryneg!=" )"):
				query="Select id, case when id in ("+queryneg+") then -1 else 0 end as val from directinfo #4"
			#	query="Select id, val FROM ("+queryneg +") Union  Select id, 0 as val From directinfo WHERE id Not In (Select id FROM ("+queryneg +"))"
			var tmpq="CREATE TABLE IF NOT EXISTS Filter_Result_"+setting.replace(" ","_").replace("+","_plus").replace(".","")+" (id INTEGER PRIMARY KEY, val FLOAT);"
			#print(setting)
			db.query(tmpq)
			db.query("Delete from Filter_Result_"+setting.replace(" ","_").replace("+","_plus").replace(".","")+";")
			var cachequery="insert into Filter_Result_"+setting.replace(" ","_").replace("+","_plus").replace(".","")+" Select id,val from ("+query.replace("#4","")+");"
			#db.query("DROP TABLE IF EXISTS Filter_Result_"+setting.replace(" ","_").replace("+","_plus")+";")
			db.query(cachequery)
			#print(cachequery)
			query="Select * from Filter_Result_"+setting.replace(" ","_").replace("+","_plus").replace(".","")
		else:
			query="Select id, case when val<-1.0 then -1.0 when val>1 then -1.0 else val end as val FROM ( " + querysort + " )"
			var cachequery="insert into Filter_Result_"+setting.replace(" ","_")+" Select id,val from ("+query.replace("#4","")+");"
			db.query("CREATE TABLE IF NOT EXISTS Filter_Result_"+setting.replace(" ","_")+" (id INTEGER PRIMARY KEY, val FLOAT);")
			db.query("Delete from Filter_Result_"+setting.replace(" ","_")+";")
			db.query(cachequery)
			query="Select * from Filter_Result_"+setting.replace(" ","_")
			pass
			#TODO if val is bigger than one this should be 0
		filters[setting]["query"]=query
		var results=db.fetch_array("Select * from ("+query+");")
		#print(results)
		pass
		#filters[setting]["positive_query"]=querypos+";"
		#filters[setting]["negative_query"]="Select id, value From " + queryneg + " Where id Not In " + querypos+";"
func sethardwhere(settingname,add):
	if(add):
		hardwheres[settingname]=" id in (Select id from ("+filters[settingname]["query"]+") where val>=0)"
	else:
		hardwheres.erase(settingname)

func updatebasequery(filterSettings):
	filterpositions=[[],[],[]]
	var basequery="Select directinfo.id as id, directinfo.name as name, directinfo.beschreibung as beschreibung, directinfo.zutatenraw as zutatenraw, directinfo.preis as preis, directinfo.menge as menge, shop.name as shop, creator.name as creator, directinfo.energie as energie, directinfo.kohlenhydrate as kohy, directinfo.eiweis as eiweis, directinfo.zucker as zucker, directinfo.fett as fett, directinfo.salz as salz, directinfo.carbon_footprint as co2#1 ,#3 from directinfo #2 left join shop on directinfo.ladenid=shop.id left join creator on directinfo.herstellerid=creator.id"
	hasfilters=false
	
	for key in range(len(filterSettings[0])):
		#var prio=filterSettings[key].get_position_in_parent()-1
		var prio=filterSettings[2][key]
		filterpositions[2].append(prio)
		filterpositions[1].append(filterSettings[1][key])
		filterpositions[0].append(filterSettings[0][key])
		#filterpositions[key]=prio
		if not key in hardwheres.keys():
			hasfilters=true
			var factor=prio#1.0-float(prio)/float(len(filterSettings)+1)
			basequery=basequery.replace("#2","Join ("+filters[filterSettings[0][key]]["query"]+") AS "+filterSettings[0][key].replace(" ","_")+" on "+filterSettings[0][key].replace(" ","_")+".id=directinfo.id #2")
			basequery=basequery.replace("#1",", "+filterSettings[0][key].replace(" ","_")+".val as "+filterSettings[0][key].replace(" ","_")+"_val#1")
			basequery=basequery.replace("#3",filterSettings[0][key].replace(" ","_")+".val * "+str(factor)+" +#3")
	basequery=basequery.replace("#1","")
	basequery=basequery.replace("#2","")
	basequery=basequery.replace("+#3"," AS summval")
	basequery=basequery.replace(",#3","")
	actualquery=basequery
	emit_signal("updatedquery",filterpositions)
	pass

signal updated_shops

func shops_select(sel_shops):
	selected_shops=sel_shops
	#print("global: "+str(sel_shops))
	emit_signal("updated_shops",sel_shops)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

class filtersorter:
	static func sort_filters(a, b):
		if a.prio < b.prio:
			return true
		return false
