extends Panel

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var id=-1
var productname
var productshop

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func setSelected(toggle):
	$Control/TextureButton.pressed=toggle

func hideSelected():
	$Control/TextureButton.visible=false

func setvalues(dictionary):
	for key in dictionary:
		if(typeof(key)==TYPE_STRING):
			if(key=="id"):
				id=dictionary["id"]
			if(key=="shop"):
				$Control/Panel.setPanelName(dictionary["shop"].replace("Lozuka/","").replace("Nord",""))
				productshop=dictionary["shop"].replace("Nord","")
			if(key=="name"):
				$Control/Label.text=dictionary["name"]
				$Control.hint_tooltip=dictionary["name"]
				productname=dictionary["name"]
			if(key=="preis"):
				$Control/VBoxContainer/Label.text=str(dictionary["preis"])+" €"
			if(key=="menge"):
				$Control/VBoxContainer/Label2.text=str(dictionary["menge"])+" Kg"
			if(key=="co2" and dictionary["co2"]!=-1 and dictionary["co2"]!=15870):
				$Control/Label2.text=str(dictionary["co2"]/1000)+"\nKg CO2 /Kg"
				$Control/Label2.hint_tooltip="CO2 Fußabdruck pro Kilo anhand von Daten des Öko instituts geschätzt"
				if(dictionary["co2"]==0):
					$Control/Label2.hint_tooltip="Der Hersteller kompensiert bekanntermaßen den CO2 Fußabdruck seiner Produkte"
			if(key.ends_with("_val") and not(key=="CO2eq_val" or key=="Geld_val")):
				$Control/FiltersList.updateFilter(key.left(len(key)-4),dictionary[key])
#	if "filters" in dictionary:
#		for filter in dictionary["filters"]:
#			$Control/Label3.addFilter(filter)
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

signal pressedInfo

func _on_Panel_gui_input(event):
	
	if( event.is_pressed() and event.button_index == BUTTON_LEFT):
		#print("guiinput")
		emit_signal("pressedInfo",id)
	pass # Replace with function body.

signal toggledInfo

func _on_TextureButton_toggled(button_pressed):
	#print("guiinput2")
	emit_signal("toggledInfo",id,button_pressed)
	pass # Replace with function body.
